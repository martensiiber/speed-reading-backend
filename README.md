# Kiirlugemise tarkvara back-end REST service

[![pipeline status](https://gitlab.com/martensiiber/speed-reading-backend/badges/dev/pipeline.svg)](https://gitlab.com/martensiiber/speed-reading-backend/commits/dev)
[![coverage report](https://gitlab.com/martensiiber/speed-reading-backend/badges/dev/coverage.svg)](https://martensiiber.gitlab.io/speed-reading-backend/coverage/)

## Requirements
* Python 3.5
* EstNLTK 1.4.1 ([Installation guide](https://github.com/estnltk/estnltk#version-141)) toolkit
* [pipenv tool](https://github.com/pypa/pipenv)
* PostgreSQL 10.6 or newer

## Setup
### Add new PostgreSQL database

### Use credentials.py.example to create credentials.py

### Install necessary modules
> pipenv install

### Initialize Flask on Windows
> SET FLASK_ENV=development
> SET FLASK_APP=api.py

### or on Linux
> export FLASK_ENV=development
> export FLASK_APP=api.py

### Create database tables
```
> pipenv run python
>>> from api import db
>>> db.drop_all()
>>> db.create_all()
>>> exit()
```

### Initialize migrations
```
> pipenv run python -m flask db init
```

## Running the application for development

```
> pipenv run python api.py
open http://localhost:<port>/api
```

## Running the application in production (Linux)

```
> export FLASK_ENV=production
> python api.py &
```

To kill previous running process, run (Linux):
```
> kill -9 $(pidof python)
```

### Migrations
```
> pipenv run python -m flask db migrate
> pipenv run python -m flask db upgrade
```

### Tests
```
> pipenv run test
```

## [Front-end application](https://gitlab.com/martensiiber/speed-reading)
