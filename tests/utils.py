from base64 import b64encode
from time import time

def auth(client, username, password):
    auth_encoded = b64encode("{0}:{1}".format(username, password).encode('UTF-8'))
    headers = {
        'Authorization': 'Basic ' + auth_encoded.decode('UTF-8')
    }
    response = client.get("/api/login", headers=headers)
    return response.json['token'] if 'token' in response.json else None

def start_time(): # pragma: no cover
    return time()

def end_time(start): # pragma: no cover
    print('{0:.3f}ms'.format((time() - start) * 1000))
