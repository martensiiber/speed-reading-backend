# -*- coding: utf-8 -*-
import unittest
from unittest import TestCase
from pathlib import Path
from utils.utils import Utils
from api.decorators import ignore_warnings
# import json

utils = Utils()

class TestUtils(TestCase):
    def setUp(self):
        text_file_est = Path('./tests/data/test_input_text_est.txt')
        text_file_eng = Path('./tests/data/test_input_text_eng.txt')
        self.text_est = text_file_est.read_text(encoding='UTF-8')
        self.text_eng = text_file_eng.read_text(encoding='UTF-8')

    def test_analyze_text_est_returns_character_count(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(result['characterCount'], 2469)

    def test_analyze_text_est_returns_word_count(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(result['wordCount'], 459)

    def test_analyze_text_est_returns_sentence_count(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(result['sentenceCount'], 19)

    def test_analyze_text_est_returns_average_word_length(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertAlmostEqual(result['averageWordLength'], 5.38, places=2)

    def test_analyze_text_est_returns_word_length_class_rating(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(result['wordLengthClassRating'], 3)

    def test_analyze_text_est_returns_average_sentence_length_in_words(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertAlmostEqual(result['averageSentenceLengthInWords'], 24.16, places=2)

    def test_analyze_text_est_returns_average_sentence_length_in_characters(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertAlmostEqual(result['averageSentenceLengthInCharacters'], 129.95, places=2)

    def test_analyze_text_est_returns_sentence_length_class_rating(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(result['sentenceLengthClassRating'], 12)

    def test_analyze_text_est_returns_word_lengths(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(len(result['wordLengths']), 12)

    def test_analyze_text_est_returns_sentence_lengths(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(len(result['sentenceLengths']), 16)

    def test_analyze_text_est_returns_word_type_counts(self):
        result = utils.analyze(self.text_est, 'estonian')
        self.assertEqual(len(result['wordTypeCounts']), 14)

    def test_analyze_text_est_returns_word_analysis(self):
        result = utils.analyze(self.text_est, 'estonian')
        # print(json.dumps(result['wordAnalysis'], indent=2, ensure_ascii=False))
        self.assertEqual(len(result['wordAnalysis']), 459)

    def test_analyze_text_est_returns_on_empty_text(self):
        result = utils.analyze('', 'estonian')
        self.assertEqual(len(result), 12)

    def test_analyze_word_est_returns_on_empty_word(self):
        result = utils.analyze_word('', None, 'estonian')
        self.assertEqual(len(result), 0)

    @ignore_warnings
    def test_analyze_text_eng_returns_character_count(self):
        result = utils.analyze(self.text_eng, 'english')
        # 1985 characters with Estonian analysis
        self.assertEqual(result['characterCount'], 1987)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_count(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(result['wordCount'], 382)

    @ignore_warnings
    def test_analyze_text_eng_returns_sentence_count(self):
        result = utils.analyze(self.text_eng, 'english')
        # 19 sentences with Estonian analysis
        self.assertEqual(result['sentenceCount'], 20)

    @ignore_warnings
    def test_analyze_text_eng_returns_average_word_length(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertAlmostEqual(result['averageWordLength'], 5.20, places=2)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_length_class_rating(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(result['wordLengthClassRating'], None)

    @ignore_warnings
    def test_analyze_text_eng_returns_average_sentence_length_in_words(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertAlmostEqual(result['averageSentenceLengthInWords'], 19.1, places=2)

    @ignore_warnings
    def test_analyze_text_eng_returns_average_sentence_length_in_characters(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertAlmostEqual(result['averageSentenceLengthInCharacters'], 99.35, places=2)

    @ignore_warnings
    def test_analyze_text_eng_returns_sentence_length_class_rating(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(result['sentenceLengthClassRating'], None)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_lengths(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(len(result['wordLengths']), 14)

    @ignore_warnings
    def test_analyze_text_eng_returns_sentence_lengths(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(len(result['sentenceLengths']), 15)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_type_counts(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(len(result['wordTypeCounts']), 25)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_analysis(self):
        result = utils.analyze(self.text_eng, 'english')
        # print(result['wordAnalysis'])
        self.assertEqual(len(result['wordAnalysis']), 382)

    @ignore_warnings
    def test_analyze_text_eng_returns_word_pair_concreteness(self):
        result = utils.analyze(self.text_eng, 'english')
        self.assertEqual(len(result['wordPairConcreteness']), 3)

    @ignore_warnings
    def test_analyze_text_eng_returns_on_empty_text(self):
        result = utils.analyze('', 'english')
        # print(result)
        self.assertEqual(len(result), 13)

    @ignore_warnings
    def test_analyze_word_eng_returns_on_empty_word(self):
        result = utils.analyze_word('test', None, 'english')
        # print(result)
        self.assertEqual(len(result), 8)

    def test_word_length_first_class_rating(self):
        class_rating = utils.get_word_length_class_rating(4.94)
        self.assertEqual(class_rating, 1)

    def test_word_length_second_class_rating(self):
        class_rating = utils.get_word_length_class_rating(5.1)
        self.assertEqual(class_rating, 2)

    def test_sentence_length_first_class_rating(self):
        class_rating = utils.get_sentence_length_class_rating(3.99)
        self.assertEqual(class_rating, 1)

    def test_sentence_length_second_class_rating(self):
        class_rating = utils.get_sentence_length_class_rating(5)
        self.assertEqual(class_rating, 2)

    def test_get_freq_dict_est(self):
        self.assertEqual(len(utils.freq_dict_est), 82457)

    def test_get_abst_dict_est(self):
        self.assertEqual(len(utils.base_abst_dict_est), 15436)

    def test_get_abst_dict_eng(self):
        self.assertEqual(len(utils.base_abst_dict_eng), 39954)

    def test_generate_blank_exercises(self):
        blank_exercises = utils.generate_blank_exercises(self.text_est)
        self.assertEqual(len(blank_exercises), 8)
        for blank_exercise in blank_exercises:
            self.assertEqual(len(blank_exercise['blankExercise']), 3)
            self.assertTrue(isinstance(blank_exercise['correct'], str))

if __name__ == '__main__':
    unittest.main()