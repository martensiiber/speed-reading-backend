# -*- coding: utf-8 -*-
import unittest
# from time import time
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from tests.utils import auth
import credentials

class TestAPI(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def setUp(self):
        # start = time()
        db.create_all()
        # diff = time() - start
        # print("API Test Setup (%.3fs)" % diff)

    def test_users_setup(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get("/api/users", headers={'x-access-token': token})
        self.assertEqual(len(response.json), 4)

    def test_reading_text_collections_setup(self):
        response = self.client.get("/api/collections")
        self.assertEqual(len(response.json), 5)

    def test_exercises_setup(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get("/api/exercises", headers={'x-access-token': token})
        self.assertEqual(len(response.json), 9)

    def tearDown(self):
        # start = time()
        db.session.close()
        db.drop_all()
        # diff = time() - start
        # print("API Test Teardown (%.3fs)" % diff)

if __name__ == '__main__':
    unittest.main()
