from api.models import ReadingText

reading_text_data_eng = {
    'collection_id': 1,
    'title': 'Test title',
    'author': 'Test author',
    'plain_text': 'Test text',
    'language': 'english',
    'content_state': [],
    'complexity': 5,
    'keywords': ['test'],
    'statistics': {
        'characterCount': 8,
        'wordCount': 2,
        'sentenceCount': 1,
        'wordLengthClassRating': None,
        'sentenceLengthClassRating': None,
        'averageInterestingnessRating': 5,
        'interestingnessRatingCount': 1,
        'averageTestDifficultyRating': 5,
        'testDifficultyRatingCount': 1,
        'totalReadingAttemptCount': 0
    }
}

reading_text_data_est = {
    'collection_id': 1,
    'title': 'Test pealkiri',
    'author': 'Test autor',
    'plain_text': 'Test tekst',
    'language': 'estonian',
    'content_state': [],
    'complexity': 5,
    'keywords': ['test'],
    'statistics': {
        'characterCount': 9,
        'wordCount': 2,
        'sentenceCount': 1,
        'wordLengthClassRating': 1,
        'sentenceLengthClassRating': 1,
        'averageInterestingnessRating': 5,
        'interestingnessRatingCount': 1,
        'averageTestDifficultyRating': 5,
        'testDifficultyRatingCount': 1,
        'totalReadingAttemptCount': 1
    }
}
