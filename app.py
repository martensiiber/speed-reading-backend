from api import create_app
from api.config import get_run_env_config

app = create_app(get_run_env_config())

if __name__ == '__main__':
    app.run()
