# -*- coding: utf-8 -*-
import unittest
import json
from flask import request, jsonify
from flask_testing import TestCase
from tests.utils import auth
from api import create_app, db
from api.config import get_test_env_config
from api.models import User, UserRole
from api.decorators import auth_required
import credentials
from tests.data import test_credentials

class TestDecorators(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def route(self, current_user):
        return jsonify(), 200

    def test_auth_required_token_missing(self):
        response, status = auth_required()(self.route)()
        self.assertEqual(response.json, {'error': 'Authentication token is missing'})
        self.assertEqual(status, 401)

    def test_auth_required_token_invalid(self):
        with self.client.application.test_request_context(headers={'x-access-token': 'invalid_token'}):
            response, status = auth_required()(self.route)()
        self.assertEqual(response.json, {'error': 'Authentication token is invalid'})
        self.assertEqual(status, 401)

    def test_auth_required_token_no_permission(self):
        token = auth(self.client, credentials.demo['username'], credentials.demo['password'])
        with self.client.application.test_request_context(headers={'x-access-token': token}):
            response, status = auth_required(minimum_role=UserRole.admin)(self.route)()
        self.assertEqual(response.json, {'error': 'You do not have required permission'})
        self.assertEqual(status, 403)

    def test_auth_required_allowed_role(self):
        token = auth(self.client, test_credentials.statistician['username'], test_credentials.statistician['password'])
        with self.client.application.test_request_context(headers={'x-access-token': token}):
            response, status = auth_required(
                minimum_role=UserRole.admin,
                allowed_roles=[UserRole.statistician]
            )(self.route)()
        self.assertEqual(response.json, {})
        self.assertEqual(status, 200)

    def test_auth_required_token_correct(self):
        token = auth(self.client, credentials.demo['username'], credentials.demo['password'])
        with self.client.application.test_request_context(headers={'x-access-token': token}):
            response, status = auth_required()(self.route)()
        self.assertEqual(response.json, {})
        self.assertEqual(status, 200)

if __name__ == '__main__':
    unittest.main()
