import sys
import logging
import credentials

class Logger():
    own_logger = logging.getLogger(__name__)

    def init_app(self, app):
        # Disable Werkzeug logger
        werkzeug_logger = logging.getLogger('werkzeug')
        werkzeug_logger.setLevel(logging.WARN)

        self.own_logger.setLevel(logging.DEBUG)
        if not app.testing:
            self.own_logger.addHandler(logging.StreamHandler(sys.stdout))

        if not app.debug:
            from threading import Thread
            from logging.handlers import RotatingFileHandler, SMTPHandler

            file_handler = RotatingFileHandler('logs/api.log', maxBytes=1024**2, backupCount=1)
            file_handler.setLevel(logging.DEBUG)
            app.logger.addHandler(file_handler)
            werkzeug_logger.addHandler(file_handler)
            self.own_logger.addHandler(file_handler)

            class ThreadedSMTPHandler(SMTPHandler):
                def emit(self, record):
                    thread = Thread(target=SMTPHandler.emit, args=(self, record))
                    thread.start()

            mail_handler = ThreadedSMTPHandler(
                mailhost=('smtp.gmail.com', 587),
                fromaddr=credentials.debug['username'],
                toaddrs=credentials.debug['username'],
                subject='Application error',
                credentials=(credentials.debug['username'], credentials.debug['password']),
                secure=())
            mail_handler.setLevel(logging.ERROR)
            app.logger.addHandler(mail_handler)

    def log(self, message):
        self.own_logger.info(message)
