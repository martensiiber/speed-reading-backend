from enum import Enum
from datetime import datetime
from sqlalchemy.ext.mutable import MutableDict
from api.types import JSONType, StringArrayType
from api import db

class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    created_at = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    last_modified = db.Column(db.DateTime, default=datetime.utcnow, nullable=False, onupdate=datetime.utcnow)

class Group(BaseModel):
    name = db.Column(db.String(50), unique=True, nullable=False)
    teacher_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="SET NULL", use_alter=True, name="group_teacher_id_fkey"), nullable=True)
    users = db.relationship('User', primaryjoin='Group.id==User.group_id', backref='group')

class UserRole(str, Enum):
    guest = 'guest'
    student = 'student'
    statistician = 'statistician'
    editor = 'editor'
    teacher = 'teacher'
    developer = 'developer'
    admin = 'admin'

class User(BaseModel):
    public_id = db.Column(db.String(50), unique=True, nullable=False)
    group_id = db.Column(db.Integer, db.ForeignKey('group.id', onupdate="CASCADE", ondelete="RESTRICT"), nullable=True)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    name = db.Column(db.String(100), nullable=True)
    first_name = db.Column(db.String(50), nullable=True)
    last_name = db.Column(db.String(50), nullable=True)
    role = db.Column(db.Enum(UserRole), nullable=False)
    last_login = db.Column(db.DateTime, nullable=True)
    groups = db.relationship('Group', primaryjoin='User.public_id==Group.teacher_id', backref='teacher')
    achievements = db.Column(MutableDict.as_mutable(JSONType), nullable=True)
    settings = db.Column(MutableDict.as_mutable(JSONType), nullable=True)

class Feedback(BaseModel):
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    message = db.Column(db.Text)
    functionality_rating = db.Column(db.Integer)
    usability_rating = db.Column(db.Integer)
    design_rating = db.Column(db.Integer)
    __table_args__ = (db.CheckConstraint('functionality_rating >= 0 AND functionality_rating <= 5'),
                      db.CheckConstraint('usability_rating >= 0 AND usability_rating <= 5'),
                      db.CheckConstraint('design_rating >= 0 AND design_rating <= 5'), )

class ProblemType(str, Enum):
    text = 'text'
    other = 'other'

class ProblemReport(BaseModel):
    resolved = db.Column(db.Boolean, nullable=False, default=False)
    type = db.Column(db.Enum(ProblemType), nullable=True)
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    text_title = db.Column(db.String(100), nullable=True)
    description = db.Column(db.Text, nullable=True)
    screenshot_filename = db.Column(db.String(100), nullable=True)

class BugReport(BaseModel):
    resolved = db.Column(db.Boolean, nullable=False, default=False)
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    description = db.Column(db.Text, nullable=True)
    version = db.Column(db.String(25), nullable=False)
    user_agent = db.Column(db.Text, nullable=False)
    platform = db.Column(db.Text, nullable=False)
    window_dimensions = db.Column(JSONType, nullable=False)
    console_errors = db.Column(JSONType, nullable=True)
    state = db.Column(JSONType, nullable=False)
    actions = db.Column(JSONType, nullable=False)
    screenshot_filename = db.Column(db.String(100), nullable=True)

class ReadingTextCollection(BaseModel):
    title = db.Column(db.String(50), nullable=False)
    texts = db.relationship('ReadingText', backref='reading_text_collection')

class TextLanguage(str, Enum):
    estonian = 'estonian'
    english = 'english'

class ReadingText(BaseModel):
    collection_id = db.Column(db.Integer, db.ForeignKey('reading_text_collection.id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)
    title = db.Column(db.String(100), nullable=False)
    author = db.Column(db.String(50), nullable=False)
    language = db.Column(db.Enum(TextLanguage), nullable=True, default='estonian')
    editor = db.Column(db.String(50), nullable=True)
    questions_author = db.Column(db.String(50), nullable=True)
    reference = db.Column(db.String(200), nullable=True)
    plain_text = db.Column(db.Text, nullable=False)
    content_state = db.Column(JSONType, nullable=False)
    complexity = db.Column(db.Integer, nullable=False)
    keywords = db.Column(StringArrayType(db.String(50)), nullable=False)
    statistics = db.Column(MutableDict.as_mutable(JSONType), nullable=False)
    questions = db.relationship('Question', backref='reading_text', passive_deletes=True)
    __table_args__ = (db.CheckConstraint('complexity >= 0 AND complexity <= 10'), )

class ExerciseType(str, Enum):
    reading = 'reading'
    help = 'help'

class Exercise(BaseModel):
    name = db.Column(db.String(50), unique=True, nullable=False)
    type = db.Column(db.Enum(ExerciseType), nullable=True)

class TestAttempt(BaseModel):
    exercise_attempt_id = db.Column(db.Integer, db.ForeignKey('exercise_attempt.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    start_time = db.Column(db.DateTime, nullable=False)
    result = db.Column(MutableDict.as_mutable(JSONType), nullable=True)

class ExerciseAttempt(BaseModel):
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    exercise_id = db.Column(db.Integer, db.ForeignKey('exercise.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    reading_text_id = db.Column(db.Integer, db.ForeignKey('reading_text.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=True)
    modification = db.Column(db.String(50), default='default', nullable=False)
    start_time = db.Column(db.DateTime, nullable=False)
    save = db.Column(db.Boolean, default=True, nullable=False)
    settings = db.Column(MutableDict.as_mutable(JSONType), nullable=True)
    result = db.Column(MutableDict.as_mutable(JSONType), nullable=True)
    exercise = db.relationship('Exercise', backref=db.backref('exercise_attempts'), uselist=False, lazy='subquery')
    reading_text = db.relationship('ReadingText', backref=db.backref('exercise_attempts'), uselist=False, lazy='subquery')
    test = db.relationship('TestAttempt', backref=db.backref('exercise_attempt'), uselist=False, lazy='subquery')
    statistics = db.Column(MutableDict.as_mutable(JSONType), nullable=True)

class QuestionCategory(str, Enum):
    question = 'question'
    blank = 'blank'

class Question(BaseModel):
    reading_text_id = db.Column(db.Integer, db.ForeignKey('reading_text.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    category = db.Column(db.Enum(QuestionCategory), nullable=False)
    question_text = db.Column(db.String(200), nullable=False)
    answers = db.relationship('Answer', backref='question', passive_deletes=True)

class Answer(BaseModel):
    question_id = db.Column(db.Integer, db.ForeignKey('question.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    answer_text = db.Column(db.String(150), nullable=False)
    correct = db.Column(db.Boolean, nullable=False)

class TestQuestionAnswer(BaseModel):
    test_attempt_id = db.Column(db.Integer, db.ForeignKey('test_attempt.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    answer_id = db.Column(db.Integer, db.ForeignKey('answer.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=True)
    __table_args__ = (db.Index('test_question_answer_index', 'test_attempt_id', 'question_id', 'answer_id', unique=True, postgresql_where=(answer_id.isnot(None))),
                      db.Index('test_question_index', 'test_attempt_id', 'question_id', unique=True, postgresql_where=(answer_id.is_(None))), )

class BlankAnswerCategory(str, Enum):
    unanswered = 'unanswered'
    correct = 'correct'
    incorrect = 'incorrect'
    misspelled = 'misspelled'
    synonym = 'synonym'

class TestBlankAnswer(BaseModel):
    test_attempt_id = db.Column(db.Integer, db.ForeignKey('test_attempt.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    language = db.Column(db.Enum(TextLanguage), nullable=False, default='estonian')
    blank_exercise = db.Column(StringArrayType(db.Text), nullable=False)
    correct = db.Column(db.String(100), nullable=False)
    answer = db.Column(db.String(100), nullable=True)
    auto_evaluation = db.Column(db.Enum(BlankAnswerCategory), nullable=False)
    user_evaluation = db.Column(db.Enum(BlankAnswerCategory), nullable=True)

class UserTextRating(BaseModel):
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    reading_text_id = db.Column(db.Integer, db.ForeignKey('reading_text.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    complexity_rating = db.Column(db.Integer)
    interestingness_rating = db.Column(db.Integer)
    __table_args__ = (db.CheckConstraint('complexity_rating >= 0 AND complexity_rating <= 10'),
                      db.Index('user_text_rating_index', 'user_id', 'reading_text_id', unique=True), )

class UserTestRating(BaseModel):
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    reading_text_id = db.Column(db.Integer, db.ForeignKey('reading_text.id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    difficulty_rating = db.Column(db.Integer)
    __table_args__ = (db.CheckConstraint('difficulty_rating >= 0 AND difficulty_rating <= 10'),
                      db.Index('user_test_rating_index', 'user_id', 'reading_text_id', unique=True), )

class ApplicationStatistics(BaseModel):
    exercise_attempt_count = db.Column(db.Integer, nullable=False)
    reading_text_count = db.Column(db.Integer, nullable=False)
    question_count = db.Column(db.Integer, nullable=False)
    feedback_count = db.Column(db.Integer, nullable=False)
    user_count = db.Column(db.Integer, nullable=False)
    reading_exercise_time = db.Column(db.BigInteger, nullable=False)
    help_exercise_time = db.Column(db.BigInteger, nullable=False)
    test_time = db.Column(db.BigInteger, nullable=False)

class Abstraction(BaseModel):
    lemma = db.Column(db.String(50), nullable=False)
    abstraction_rating = db.Column(db.Integer, nullable=True)
    language = db.Column(db.Enum(TextLanguage), nullable=False, default='estonian')
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="SET NULL"), nullable=True)

class AnalyzedText(BaseModel):
    user_id = db.Column(db.String(50), db.ForeignKey('user.public_id', onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    identifier = db.Column(db.String(100), nullable=False)
    language = db.Column(db.Enum(TextLanguage), nullable=False, default='estonian')
    text = db.Column(db.Text, nullable=False)
    analysis = db.Column(JSONType, nullable=False)
