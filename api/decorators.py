from flask import request, make_response, jsonify, current_app
from functools import wraps
import jwt
import warnings
from api.models import User
from api.resources.utility.utils import role_permissions

def auth_required(minimum_role=None, allowed_roles=[]):
    def token_decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            token = None

            if 'x-access-token' in request.headers:
                token = request.headers['x-access-token']

            if not token:
                return jsonify({'error': 'Authentication token is missing'}), 401

            try:
                data = jwt.decode(token, current_app.config['SECRET_KEY'], algorithms=['HS256'])
                current_user = User.query.filter_by(public_id=data['publicId']).first()
            except:
                return jsonify({'error': 'Authentication token is invalid'}), 401

            if minimum_role and role_permissions[current_user.role] < role_permissions[minimum_role] and current_user.role not in allowed_roles:
                return jsonify({'error': 'You do not have required permission'}), 403

            return f(current_user, *args, **kwargs)
        return decorated
    return token_decorator

def expires(minutes=None): # pragma: no cover
    def expires_decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            response = make_response(f(*args, **kwargs))
            if minutes is not None:
                response.cache_control.max_age = minutes * 60
            return response
        return decorated
    return expires_decorator

def ignore_warnings(function):
    def do_method(self, *args, **kwargs):
        with warnings.catch_warnings():
            warnings.simplefilter('ignore')
            function(self, *args, **kwargs)
    return do_method
