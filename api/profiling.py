import cProfile
import io
import pstats
import contextlib

@contextlib.contextmanager
def profile():
    c_profile = cProfile.Profile()
    c_profile.enable()
    yield
    c_profile.disable()
    string_stream = io.StringIO()
    profiled_stats = pstats.Stats(c_profile, stream=string_stream).sort_stats('cumulative')
    profiled_stats.print_stats()
    # uncomment this to see who's calling what
    # ps.print_callers()
    print(string_stream.getvalue())
