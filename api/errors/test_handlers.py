# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app
from api.config import get_test_env_config
from api.errors.handlers import method_not_allowed, internal_server_error

class TestHandlers(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def test_not_found_returns_response(self):
        response = self.client.post('/api/test')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'The requested URL was not found on the server'
        })

    def test_method_not_allowed(self):
        response, status_code = method_not_allowed('Some error')
        self.assertEqual(status_code, 405)
        self.assertEqual(response.json, {
            'error': 'The method is not allowed for the requested URL'
        })

    def test_internal_server_error(self):
        response, status_code = internal_server_error('Some server error')
        self.assertEqual(status_code, 500)
        self.assertEqual(response.json, {
            'error': 'There was a problem processing the requested URL'
        })

if __name__ == '__main__':
    unittest.main()
