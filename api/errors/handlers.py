from flask import Blueprint, jsonify

errors = Blueprint('errors', __name__)

# Error handling
@errors.app_errorhandler(404)
def not_found(error):
    return jsonify({'error': 'The requested URL was not found on the server'}), 404

@errors.app_errorhandler(405)
def method_not_allowed(error):
    return jsonify({'error': 'The method is not allowed for the requested URL'}), 405

@errors.app_errorhandler(500)
def internal_server_error(error):
    return jsonify({'error': 'There was a problem processing the requested URL'}), 500
