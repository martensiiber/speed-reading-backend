from flask import Flask
from flask_caching import Cache
from flask_cors import CORS
from flask_mail import Mail
from flask_sqlalchemy import Model, SQLAlchemy
from flask_migrate import Migrate
import flask_excel as excel

from api.config import DevelopmentConfig
from api.logger import Logger
from utils.utils import Utils

db = SQLAlchemy()
migrate = Migrate()
cors = CORS()
mail = Mail()
cache = Cache()
logger = Logger()
utils = Utils()

def create_app(config_class=DevelopmentConfig):
    app = Flask(__name__)
    app.config.from_object(config_class)

    db.init_app(app)
    cache.init_app(app, config={'CACHE_TYPE': 'simple'})
    cors.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    excel.init_excel(app)
    logger.init_app(app)
    utils.init_app(app, db)

    import api.initial_setup
    if app.config['DEBUG'] and not app.config['TESTING']:
        import api.query_interceptor
    from api.resources.main.routes import main
    from api.resources.application_statistics.routes import application_statistics
    from api.resources.groups.routes import groups
    from api.resources.users.routes import users
    from api.resources.achievements.routes import achievements
    from api.resources.auth.routes import auth
    from api.resources.feedback.routes import feedback
    from api.resources.problem_reports.routes import problem_reports
    from api.resources.bug_reports.routes import bug_reports
    from api.resources.reading_text_collections.routes import reading_text_collections
    from api.resources.reading_texts.routes import reading_texts
    from api.resources.questions.routes import questions
    from api.resources.answers.routes import answers
    from api.resources.analyzed_texts.routes import analyzed_texts
    from api.resources.abstractions.routes import abstractions
    from api.resources.exercises.routes import exercises
    from api.resources.exercise_attempts.routes import exercise_attempts
    from api.resources.exercise_test_attempts.routes import test_attempts
    from api.resources.exercise_test_question_answers.routes import test_question_answers
    from api.resources.exercise_test_blank_answers.routes import test_blank_answers
    from api.resources.user_text_ratings.routes import user_text_ratings
    from api.resources.user_test_ratings.routes import user_test_ratings
    from api.errors.handlers import errors

    app.register_blueprint(main)
    app.register_blueprint(application_statistics)
    app.register_blueprint(groups)
    app.register_blueprint(users)
    app.register_blueprint(achievements)
    app.register_blueprint(auth)
    app.register_blueprint(feedback)
    app.register_blueprint(problem_reports)
    app.register_blueprint(bug_reports)
    app.register_blueprint(reading_text_collections)
    app.register_blueprint(reading_texts)
    app.register_blueprint(questions)
    app.register_blueprint(answers)
    app.register_blueprint(analyzed_texts)
    app.register_blueprint(abstractions)
    app.register_blueprint(exercises)
    app.register_blueprint(exercise_attempts)
    app.register_blueprint(test_attempts)
    app.register_blueprint(test_question_answers)
    app.register_blueprint(test_blank_answers)
    app.register_blueprint(user_text_ratings)
    app.register_blueprint(user_test_ratings)
    app.register_blueprint(errors)

    return app
