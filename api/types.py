import json
import sqlalchemy.types as types
from sqlalchemy.dialects.postgresql import JSON, ARRAY

class JSONType(types.TypeDecorator):
    """StringArrayType uses JSON type with PostgreSQL and Text for other databases"""
    impl = types.Text

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(JSON())
        else:
            return dialect.type_descriptor(self.impl)

    def process_bind_param(self, value, dialect):
        if dialect.name == 'postgresql':
            return value
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if dialect.name == 'postgresql':
            return value
        if value is not None:
            value = json.loads(value)
        return value

class StringArrayType(types.TypeDecorator):
    """StringArrayType uses JSON type with PostgreSQL and Text for other databases"""

    impl = types.Text

    def __init__(self, item_type, *args, **kwargs):
        self.item_type = item_type
        types.TypeDecorator.__init__(self, *args, **kwargs)

    def load_dialect_impl(self, dialect):
        if dialect.name == 'postgresql':
            return dialect.type_descriptor(ARRAY(self.item_type))
        else:
            return dialect.type_descriptor(self.impl)

    def process_bind_param(self, value, dialect):
        if dialect.name == 'postgresql':
            return value
        if value is not None:
            value = json.dumps(value)
        return value

    def process_result_value(self, value, dialect):
        if dialect.name == 'postgresql':
            return value
        if value is not None:
            value = json.loads(value)
        return value

# StringArrayType = types.ARRAY(types.String(50)).with_variant(StringyARRAY, 'sqlite')
