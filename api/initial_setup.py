from werkzeug.security import generate_password_hash
from flask import current_app
from sqlalchemy import event
import uuid
from api import db
from api.models import User, UserRole, ReadingTextCollection, Exercise, ExerciseType, ApplicationStatistics
import credentials
from tests.data import test_credentials

# Insert initial data after db.create_all()
@event.listens_for(User.__table__, 'after_create')
def insert_initial_users(*args, **kwargs):
    db.session.add(User(
        public_id=str(uuid.uuid4()),
        email=credentials.admin['username'],
        password=generate_password_hash(credentials.admin['password'], method='sha256'),
        name='Administrator',
        role=UserRole.admin
    ))
    db.session.add(User(
        public_id=str(uuid.uuid4()),
        email=credentials.demo['username'],
        password=generate_password_hash(credentials.demo['password'], method='sha256'),
        name='Proovikasutaja',
        role=UserRole.student
    ))
    if current_app.testing:
        db.session.add(User(
            public_id=str(uuid.uuid4()),
            email=test_credentials.teacher['username'],
            password=generate_password_hash(test_credentials.teacher['password'], method='sha256'),
            role=UserRole.teacher
        ))
        db.session.add(User(
            public_id=str(uuid.uuid4()),
            email=test_credentials.statistician['username'],
            password=generate_password_hash(test_credentials.statistician['password'], method='sha256'),
            role=UserRole.statistician
        ))
    db.session.commit()

@event.listens_for(ReadingTextCollection.__table__, 'after_create')
def insert_initial_reading_text_collections(*args, **kwargs):
    db.session.add(ReadingTextCollection(title='Kirjandus'))
    db.session.add(ReadingTextCollection(title='Kultuur'))
    db.session.add(ReadingTextCollection(title='Loodus'))
    db.session.add(ReadingTextCollection(title='Tervis'))
    db.session.add(ReadingTextCollection(title='Ühiskond'))
    db.session.commit()

@event.listens_for(Exercise.__table__, 'after_create')
def insert_initial_exercises(*args, **kwargs):
    db.session.add(Exercise(name='readingTest', type=ExerciseType.reading))
    db.session.add(Exercise(name='readingAid', type=ExerciseType.reading))
    db.session.add(Exercise(name='scrolling', type=ExerciseType.reading))
    db.session.add(Exercise(name='disappearing', type=ExerciseType.reading))
    db.session.add(Exercise(name='wordGroups', type=ExerciseType.reading))
    db.session.add(Exercise(name='schulteTables', type=ExerciseType.help))
    db.session.add(Exercise(name='concentration', type=ExerciseType.help))
    db.session.add(Exercise(name='verticalReading', type=ExerciseType.reading))
    db.session.add(Exercise(name='movingWordGroups', type=ExerciseType.reading))
    db.session.commit()

@event.listens_for(ApplicationStatistics.__table__, 'after_create')
def insert_initial_application_statistics(*args, **kwargs):
    db.session.add(ApplicationStatistics(
        exercise_attempt_count=0,
        reading_text_count=0,
        question_count=0,
        feedback_count=0,
        user_count=2,
        reading_exercise_time=0,
        help_exercise_time=0,
        test_time=0
    ))
    db.session.commit()
