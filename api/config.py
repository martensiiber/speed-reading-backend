import os
import credentials

class Config:
    DEBUG = True
    SECRET_KEY = credentials.auth['secret']
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USE_SSL = False
    MAIL_USERNAME = credentials.mail['username']
    MAIL_PASSWORD = credentials.mail['password']
    MAIL_DEFAULT_SENDER = credentials.mail['sender']

class TestConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    # SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost:5432/speedreading_test'
    PRESERVE_CONTEXT_ON_EXCEPTION = False

class StagingConfig(Config):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:@postgres:5432/speedreading_test'
    PRESERVE_CONTEXT_ON_EXCEPTION = False

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = credentials.database['development']

class ProductionConfig(Config):
    DEBUG = False
    SERVER_NAME = 'localhost:5000'
    SQLALCHEMY_DATABASE_URI = credentials.database['production']

def get_test_env_config(): # pragma: no cover
    environment = os.environ['FLASK_ENV']
    if environment == 'staging':
        return StagingConfig
    elif environment == 'development':
        return TestConfig
    raise NotImplementedError

def get_run_env_config(): # pragma: no cover
    environment = os.environ['FLASK_ENV']
    if environment == 'development':
        return DevelopmentConfig
    elif environment == 'production':
        return ProductionConfig
    raise NotImplementedError
