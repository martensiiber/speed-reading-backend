# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import Feedback, ApplicationStatistics
from tests.utils import auth
import credentials

class TestFeedback(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(Feedback(
            message='Feedback message',
            functionality_rating=5,
            usability_rating=5,
            design_rating=5
        ))
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.feedback_count = 1
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_feedback_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        feedback_data = {
            'message': 'Feedback message',
            'functionalityRating': 5,
            'usabilityRating': 5,
            'designRating': 5
        }
        response = self.client.post('/api/feedback', json=feedback_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Feedback added'
        })

    def test_add_feedback_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        feedback_data = {
            'message': 'Feedback message',
            'functionalityRating': 5,
            'usabilityRating': 5,
            'designRating': 5
        }
        self.client.post('/api/feedback', json=feedback_data, headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.feedback_count, 2)

    def test_get_feedback_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/feedback', headers={'x-access-token': token})
        feedback = Feedback.query.filter_by(id=1).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'date': feedback.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'userId': None,
            'message': 'Feedback message',
            'functionalityRating': 5,
            'usabilityRating': 5,
            'designRating': 5
        }])

if __name__ == '__main__':
    unittest.main()
