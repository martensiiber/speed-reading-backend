from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, Feedback, ApplicationStatistics
from api.decorators import auth_required

feedback = Blueprint('feedback', __name__)

# Feedback resource
@feedback.route('/api/feedback', methods=['GET'])
@auth_required(minimum_role=UserRole.admin)
def get_all_feedbacks(current_user):
    feedbacks = Feedback.query.all()

    output = []
    for feedback in feedbacks:
        feedback_data = {}
        feedback_data['id'] = feedback.id
        feedback_data['date'] = feedback.created_at
        feedback_data['userId'] = feedback.user_id
        feedback_data['message'] = feedback.message
        feedback_data['functionalityRating'] = feedback.functionality_rating
        feedback_data['usabilityRating'] = feedback.usability_rating
        feedback_data['designRating'] = feedback.design_rating
        output.append(feedback_data)
    return jsonify(output), 200

@feedback.route('/api/feedback', methods=['POST'])
def add_feedback():
    data = request.get_json()
    new_feedback = Feedback(
        message=data['message'],
        functionality_rating=data['functionalityRating'],
        usability_rating=data['usabilityRating'],
        design_rating=data['designRating']
    )
    if 'userId' in data:
        new_feedback.user_id = data['userId']
    db.session.add(new_feedback)
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.feedback_count += 1
    db.session.commit()
    return jsonify({'message': 'Feedback added'}), 201, {'Location': '/feedback/{}'.format(new_feedback.id)}
