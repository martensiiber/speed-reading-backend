from flask import Blueprint, jsonify, request
from sqlalchemy import and_
from sqlalchemy.orm.query import Bundle
from api import db, utils
from api.models import UserRole, ReadingText, ExerciseAttempt, Question, ApplicationStatistics
from api.decorators import auth_required

reading_texts = Blueprint('reading_texts', __name__)
get_reading_text_bundle = Bundle(
    'reading_text',
    ReadingText.id, ReadingText.collection_id, ReadingText.title, ReadingText.author,
    ReadingText.language, ReadingText.editor, ReadingText.questions_author, ReadingText.reference,
    ReadingText.statistics, ReadingText.complexity, ReadingText.keywords
)

@reading_texts.route('/api/texts', methods=['GET'])
@auth_required()
def get_all_reading_texts(current_user):
    reading_texts = db.session.query(
        get_reading_text_bundle,
        db.func.count(ExerciseAttempt.id),
    ).outerjoin(
        ExerciseAttempt, and_(
            ExerciseAttempt.reading_text_id == ReadingText.id,
            ExerciseAttempt.user_id == current_user.public_id
        )).group_by(ReadingText.id).order_by(ReadingText.id).all()

    output = []
    for (reading_text, text_reading_attempt_count) in reading_texts:
        reading_text_data = {}
        reading_text_data['id'] = reading_text.id
        reading_text_data['collectionId'] = reading_text.collection_id
        reading_text_data['title'] = reading_text.title
        reading_text_data['author'] = reading_text.author
        reading_text_data['language'] = reading_text.language
        reading_text_data['editor'] = reading_text.editor
        reading_text_data['questionsAuthor'] = reading_text.questions_author
        reading_text_data['reference'] = reading_text.reference
        reading_text_data['complexity'] = reading_text.complexity
        reading_text_data['keywords'] = reading_text.keywords
        reading_text_statistics = reading_text.statistics
        reading_text_statistics.update({
            'textReadingAttemptCount': text_reading_attempt_count
        })
        reading_text_data['statistics'] = reading_text_statistics
        output.append(reading_text_data)
    return jsonify(output), 200

@reading_texts.route('/api/texts/<id>', methods=['GET'])
@auth_required()
def get_one_reading_text(current_user, id):
    reading_text = ReadingText.query.filter_by(id=id).first()

    if not reading_text:
        return jsonify({'error': 'No reading text found'}), 404

    reading_text_data = {}
    reading_text_data['id'] = reading_text.id
    reading_text_data['collectionId'] = reading_text.collection_id
    reading_text_data['title'] = reading_text.title
    reading_text_data['author'] = reading_text.author
    reading_text_data['language'] = reading_text.language
    reading_text_data['editor'] = reading_text.editor
    reading_text_data['questionsAuthor'] = reading_text.questions_author
    reading_text_data['reference'] = reading_text.reference
    reading_text_data['plainText'] = reading_text.plain_text
    reading_text_data['contentState'] = reading_text.content_state
    reading_text_data['complexity'] = reading_text.complexity
    reading_text_data['keywords'] = reading_text.keywords
    reading_text_statistics = reading_text.statistics
    reading_text_statistics.update({
        'textReadingAttemptCount': ExerciseAttempt.query.filter(and_(
            ExerciseAttempt.reading_text_id == reading_text.id,
            ExerciseAttempt.user_id == current_user.public_id
        )).count()
    })
    reading_text_data['statistics'] = reading_text_statistics

    return jsonify(reading_text_data), 200

@reading_texts.route('/api/texts', methods=['POST'])
@auth_required(minimum_role=UserRole.editor)
def add_reading_text(current_user):
    data = request.get_json()
    new_reading_text = ReadingText(
        collection_id=data['collectionId'],
        title=data['title'],
        author=data['author'],
        plain_text=data['plainText'],
        content_state=data['contentState'],
        complexity=data['complexity'],
        keywords=data['keywords']
    )
    if 'language' in data:
        new_reading_text.language = data['language']
    if 'editor' in data:
        new_reading_text.editor = data['editor']
    if 'questionsAuthor' in data:
        new_reading_text.questions_author = data['questionsAuthor']
    if 'reference' in data:
        new_reading_text.reference = data['reference']
    analysis = utils.analyze(new_reading_text.plain_text, new_reading_text.language)
    new_reading_text.statistics = {
        'characterCount': analysis['characterCount'],
        'wordCount': analysis['wordCount'],
        'sentenceCount': analysis['sentenceCount'],
        'wordLengthClassRating': analysis['wordLengthClassRating'],
        'sentenceLengthClassRating': analysis['sentenceLengthClassRating'],
        'averageInterestingnessRating': None,
        'interestingnessRatingCount': 0,
        'averageTestDifficultyRating': None,
        'testDifficultyRatingCount': 0,
        'totalReadingAttemptCount': 0
    }
    db.session.add(new_reading_text)
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.reading_text_count += 1
    db.session.commit()
    return jsonify({'message': 'Reading text added'}), 201, {'Location': '/texts/{}'.format(new_reading_text.id)}

@reading_texts.route('/api/texts/<id>', methods=['PUT'])
@auth_required(minimum_role=UserRole.editor)
def update_reading_text(current_user, id):
    reading_text = ReadingText.query.filter_by(id=id).first()

    if not reading_text:
        return jsonify({'error': 'No reading text found'}), 404

    data = request.get_json()
    reading_text.collection_id = data['collectionId']
    reading_text.title = data['title']
    reading_text.author = data['author']
    reading_text.plain_text = data['plainText']
    reading_text.content_state = data['contentState']
    reading_text.complexity = data['complexity']
    reading_text.keywords = data['keywords']
    if 'language' in data:
        reading_text.language = data['language']
    if 'editor' in data:
        reading_text.editor = data['editor']
    if 'questionsAuthor' in data:
        reading_text.questions_author = data['questionsAuthor']
    if 'reference' in data:
        reading_text.reference = data['reference']
    analysis = utils.analyze(reading_text.plain_text, reading_text.language)
    reading_text.statistics.update({
        'characterCount': analysis['characterCount'],
        'wordCount': analysis['wordCount'],
        'sentenceCount': analysis['sentenceCount'],
        'wordLengthClassRating': analysis['wordLengthClassRating'],
        'sentenceLengthClassRating': analysis['sentenceLengthClassRating'],
    })

    db.session.commit()
    return jsonify({'message': 'Reading text updated'}), 200

@reading_texts.route('/api/texts/<id>/questions', methods=['GET'])
def get_reading_text_questions(id):
    reading_text = ReadingText.query.filter_by(id=id).first()

    if not reading_text:
        return jsonify({'error': 'No reading text found'}), 404

    questions = Question.query.filter_by(reading_text_id=reading_text.id).all()

    output = []
    for question in questions:
        question_data = {}
        question_data['id'] = question.id
        question_data['readingTextId'] = question.reading_text_id
        question_data['category'] = question.category
        question_data['questionText'] = question.question_text
        output.append(question_data)

    return jsonify(output), 200
