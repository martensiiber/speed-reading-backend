# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from flask_testing import TestCase
from api import create_app, db, utils
from api.config import get_test_env_config
from api.models import ReadingText, User, ExerciseAttempt, ApplicationStatistics, Question, QuestionCategory
from tests.utils import auth
from tests.data.resources import reading_text_data_eng, reading_text_data_est
import credentials

class TestReadingTexts(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(ReadingText(**reading_text_data_eng))
        db.session.add(ReadingText(**reading_text_data_est))
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=1,
            modification='default',
            start_time=datetime.now(),
            reading_text_id=2
        ))
        db.session.add(Question(
            reading_text_id=2,
            category=QuestionCategory.question,
            question_text='Test question'
        ))
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.reading_text_count = 2
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_reading_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        text_data = {
            'collectionId': 1,
            'title': 'Test title',
            'author': 'Test author',
            'plainText': 'Some text',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test']
        }
        response = self.client.post('/api/texts', json=text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Reading text added'
        })

    def test_add_reading_text_analyzes_text(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        text_data = {
            'collectionId': 1,
            'title': 'Pealkiri',
            'author': 'Autor',
            'plainText': 'Tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test'],
            'language': 'estonian'
        }
        self.client.post('/api/texts', json=text_data, headers={'x-access-token': token})
        reading_text = ReadingText.query.filter_by(title='Pealkiri').first()
        analysis = utils.analyze('Tekst', 'estonian')
        self.assertEqual(reading_text.statistics, {
            'characterCount': analysis['characterCount'],
            'wordCount': analysis['wordCount'],
            'sentenceCount': analysis['sentenceCount'],
            'wordLengthClassRating': analysis['wordLengthClassRating'],
            'sentenceLengthClassRating': analysis['sentenceLengthClassRating'],
            'averageInterestingnessRating': None,
            'interestingnessRatingCount': 0,
            'averageTestDifficultyRating': None,
            'testDifficultyRatingCount': 0,
            'totalReadingAttemptCount': 0,
        })

    def test_add_reading_text_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        text_data = {
            'collectionId': 1,
            'title': 'Pealkiri',
            'author': 'Autor',
            'plainText': 'Tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test'],
            'language': 'estonian'
        }
        self.client.post('/api/texts', json=text_data, headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.reading_text_count, 3)

    def test_update_reading_text_returns_response(self):
        text_data = {
            'collectionId': 1,
            'title': 'Test pealkiri',
            'author': 'Test autor',
            'plainText': 'Muudetud test tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test']
        }
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.put('/api/texts/2', json=text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Reading text updated'
        })

    def test_update_unknown_reading_text_returns_not_found(self):
        text_data = {
            'collectionId': 1,
            'title': 'Test pealkiri',
            'author': 'Test autor',
            'plainText': 'Muudetud test tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test']
        }
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.put('/api/texts/0', json=text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No reading text found'
        })

    def test_update_reading_text_reanalyzes_text(self):
        text_data = {
            'collectionId': 1,
            'title': 'Test pealkiri',
            'author': 'Test autor',
            'plainText': 'Muudetud test tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test']
        }
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        self.client.put('/api/texts/2', json=text_data, headers={'x-access-token': token})
        reading_text = ReadingText.query.filter_by(title='Test pealkiri').first()
        analysis = utils.analyze('Muudetud test tekst', 'estonian')
        self.assertEqual(reading_text.statistics, {
            'characterCount': analysis['characterCount'],
            'wordCount': analysis['wordCount'],
            'sentenceCount': analysis['sentenceCount'],
            'wordLengthClassRating': analysis['wordLengthClassRating'],
            'sentenceLengthClassRating': analysis['sentenceLengthClassRating'],
            'averageInterestingnessRating': 5,
            'interestingnessRatingCount': 1,
            'averageTestDifficultyRating': 5,
            'testDifficultyRatingCount': 1,
            'totalReadingAttemptCount': 1,
        })

    def test_get_one_reading_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/texts/2', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 2,
            'collectionId': 1,
            'title': 'Test pealkiri',
            'author': 'Test autor',
            'language': 'estonian',
            'editor': None,
            'questionsAuthor': None,
            'reference': None,
            'plainText': 'Test tekst',
            'contentState': [],
            'complexity': 5,
            'keywords': ['test'],
            'statistics': {
                'characterCount': 9,
                'wordCount': 2,
                'sentenceCount': 1,
                'wordLengthClassRating': 1,
                'sentenceLengthClassRating': 1,
                'totalReadingAttemptCount': 1,
                'averageInterestingnessRating': 5,
                'interestingnessRatingCount': 1,
                'averageTestDifficultyRating': 5,
                'testDifficultyRatingCount': 1,
                'textReadingAttemptCount': 1
            },
        })

    def test_get_unknown_reading_text_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/texts/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No reading text found'
        })

    def test_get_reading_texts_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/texts', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 2)
        self.assertEqual(response.json[0], {
            'id': 1,
            'collectionId': 1,
            'title': 'Test title',
            'author': 'Test author',
            'language': 'english',
            'editor': None,
            'questionsAuthor': None,
            'reference': None,
            'complexity': 5,
            'keywords': ['test'],
            'statistics': {
                'characterCount': 8,
                'wordCount': 2,
                'sentenceCount': 1,
                'wordLengthClassRating': None,
                'sentenceLengthClassRating': None,
                'averageInterestingnessRating': 5,
                'interestingnessRatingCount': 1,
                'averageTestDifficultyRating': 5,
                'testDifficultyRatingCount': 1,
                'totalReadingAttemptCount': 0,
                'textReadingAttemptCount': 0
            },
        })
        self.assertEqual(response.json[1], {
            'id': 2,
            'collectionId': 1,
            'title': 'Test pealkiri',
            'author': 'Test autor',
            'language': 'estonian',
            'editor': None,
            'questionsAuthor': None,
            'reference': None,
            'complexity': 5,
            'keywords': ['test'],
            'statistics': {
                'characterCount': 9,
                'wordCount': 2,
                'sentenceCount': 1,
                'wordLengthClassRating': 1,
                'sentenceLengthClassRating': 1,
                'averageInterestingnessRating': 5,
                'interestingnessRatingCount': 1,
                'averageTestDifficultyRating': 5,
                'testDifficultyRatingCount': 1,
                'totalReadingAttemptCount': 1,
                'textReadingAttemptCount': 1
            },
        })

    def test_get_reading_text_questions_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/texts/2/questions', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'readingTextId': 2,
            'category': 'question',
            'questionText': 'Test question'
        }])

    def test_get_unknown_reading_text_questions_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/texts/0/questions', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No reading text found'
        })

if __name__ == '__main__':
    unittest.main()
