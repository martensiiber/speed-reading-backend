# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, UserTestRating, User
from tests.utils import auth
from tests.data.resources import reading_text_data_eng, reading_text_data_est
import credentials

class TestUserTestRatings(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        admin_user = User.query.filter_by(email=credentials.admin['username']).first()
        demo_user = User.query.filter_by(email=credentials.demo['username']).first()
        db.session.add(ReadingText(**reading_text_data_eng))
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.commit()
        db.session.add(UserTestRating(
            user_id=demo_user.public_id,
            reading_text_id=1,
            difficulty_rating=5
        ))
        db.session.add(UserTestRating(
            user_id=admin_user.public_id,
            reading_text_id=2,
            difficulty_rating=5
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_test_rating_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email=credentials.admin['username']).first()
        test_rating_data = {
            'userId': user.public_id,
            'readingTextId': 1,
            'difficultyRating': 5
        }
        response = self.client.post('/api/testRatings', json=test_rating_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 3,
            'message': 'Test rating added'
        })

    def test_add_test_rating_updates_reading_text_test_difficulty_rating(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email=credentials.admin['username']).first()
        reading_text_id = 1
        test_rating_data = {
            'userId': user.public_id,
            'readingTextId': reading_text_id,
            'difficultyRating': 10
        }
        self.client.post('/api/testRatings', json=test_rating_data, headers={'x-access-token': token})
        reading_text = ReadingText.query.filter_by(id=reading_text_id).first()
        self.assertEqual(reading_text.statistics['testDifficultyRatingCount'], 2)
        self.assertEqual(reading_text.statistics['averageTestDifficultyRating'], 7.5)

    def test_update_test_rating_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email=credentials.admin['username']).first()
        test_rating_data = {
            'userId': user.public_id,
            'readingTextId': 2,
            'difficultyRating': 10
        }
        response = self.client.post('/api/testRatings', json=test_rating_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Test rating updated'
        })

    def test_update_test_rating_updates_reading_text_test_difficulty_rating(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email=credentials.admin['username']).first()
        reading_text_id = 2
        test_rating_data = {
            'userId': user.public_id,
            'readingTextId': reading_text_id,
            'difficultyRating': 10
        }
        self.client.post('/api/testRatings', json=test_rating_data, headers={'x-access-token': token})
        reading_text = ReadingText.query.filter_by(id=reading_text_id).first()
        self.assertEqual(reading_text.statistics['testDifficultyRatingCount'], 1)
        self.assertEqual(reading_text.statistics['averageTestDifficultyRating'], 10)

if __name__ == '__main__':
    unittest.main()
