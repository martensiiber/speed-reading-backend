from flask import Blueprint, jsonify, request
from sqlalchemy import and_
from api import db
from api.models import UserRole, UserTestRating, ReadingText
from api.decorators import auth_required

user_test_ratings = Blueprint('test_ratings', __name__)

@user_test_ratings.route('/api/testRatings', methods=['POST'])
@auth_required(minimum_role=UserRole.student)
def add_test_rating(current_user):
    data = request.get_json()
    user_id = current_user.public_id
    reading_text_id = data['readingTextId']
    difficulty_rating = data['difficultyRating']

    reading_text = ReadingText.query.filter_by(id=reading_text_id).first()

    test_rating = UserTestRating.query.filter(and_(
        UserTestRating.user_id == user_id,
        UserTestRating.reading_text_id == reading_text_id
    )).first()

    if not test_rating:
        new_test_rating = UserTestRating(
            user_id=user_id,
            reading_text_id=reading_text_id,
            difficulty_rating=difficulty_rating
        )
        db.session.add(new_test_rating)
        db.session.commit()
        result = jsonify({'message': 'Test rating added', 'id': new_test_rating.id}), 201, {'Location': '/testRatings/{}'.format(new_test_rating.id)}
    else:
        test_rating.difficulty_rating = difficulty_rating
        db.session.commit()
        result = jsonify({'message': 'Test rating updated'}), 200

    # Update reading text statistics
    (test_difficulty_rating_count, average_test_difficulty_rating) = db.session.query(
        db.func.count(UserTestRating.id),
        db.func.avg(UserTestRating.difficulty_rating)
    ).filter(
        UserTestRating.reading_text_id == reading_text.id,
    ).first()

    reading_text.statistics.update({
        'testDifficultyRatingCount': test_difficulty_rating_count,
        'averageTestDifficultyRating': float(average_test_difficulty_rating)
    })

    return result
