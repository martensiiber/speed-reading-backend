from flask import Blueprint, jsonify, request
from sqlalchemy import and_
from api import db
from api.models import UserRole, UserTextRating, ReadingText
from api.decorators import auth_required

user_text_ratings = Blueprint('text_ratings', __name__)

@user_text_ratings.route('/api/textRatings', methods=['POST'])
@auth_required(minimum_role=UserRole.student)
def add_text_rating(current_user):
    data = request.get_json()
    user_id = current_user.public_id
    reading_text_id = data['readingTextId']
    interestingness_rating = data['interestingnessRating']

    reading_text = ReadingText.query.filter_by(id=reading_text_id).first()

    text_rating = UserTextRating.query.filter(and_(
        UserTextRating.user_id == user_id,
        UserTextRating.reading_text_id == reading_text_id
    )).first()

    if not text_rating:
        new_text_rating = UserTextRating(
            user_id=user_id,
            reading_text_id=reading_text_id,
            interestingness_rating=interestingness_rating
        )
        db.session.add(new_text_rating)
        db.session.commit()
        result = jsonify({'message': 'Text rating added', 'id': new_text_rating.id}), 201, {'Location': '/textRatings/{}'.format(new_text_rating.id)}
    else:
        text_rating.interestingness_rating = interestingness_rating
        db.session.commit()
        result = jsonify({'message': 'Text rating updated'}), 200

    # Update reading text statistics
    (interestingness_rating_count, average_interestingness_rating) = db.session.query(
        db.func.count(UserTextRating.id),
        db.func.avg(UserTextRating.interestingness_rating)
    ).filter(
        UserTextRating.reading_text_id == reading_text.id,
    ).first()

    reading_text.statistics.update({
        'interestingnessRatingCount': interestingness_rating_count,
        'averageInterestingnessRating': float(average_interestingness_rating)
    })

    db.session.commit()

    return result
