from flask import Blueprint, jsonify, request
from werkzeug.security import generate_password_hash
import uuid
from api import db
from api.models import User, UserRole, ApplicationStatistics
from api.decorators import auth_required
from api.resources.utility.utils import role_permissions, generate_password, send_password_email

users = Blueprint('users', __name__)

@users.route('/api/users', methods=['GET'])
@auth_required(minimum_role=UserRole.teacher, allowed_roles=[UserRole.statistician])
def get_all_users(current_user):
    users = User.query.order_by(User.first_name.asc(), User.email.asc()).all()

    current_user_group_ids = [group.id for group in current_user.groups]
    output = []
    for user in users:
        anonymous_identifier = str(user.public_id[-3:]).rjust(7, '#')
        show_personal_data = (
            current_user.role != UserRole.statistician and (
                current_user.role != UserRole.teacher or
                current_user.role == UserRole.teacher and
                user.group_id in current_user_group_ids
            ) or user.public_id == current_user.public_id
        )
        user_data = {}
        user_data['publicId'] = user.public_id
        user_data['groupId'] = user.group_id
        user_data['firstName'] = user.first_name if show_personal_data else anonymous_identifier
        user_data['lastName'] = user.last_name if show_personal_data else anonymous_identifier
        user_data['email'] = user.email if show_personal_data else anonymous_identifier + '@gmail.com'
        user_data['registrationDate'] = user.created_at
        user_data['lastLogin'] = user.last_login
        user_data['role'] = user.role
        user_data['achievements'] = user.achievements
        user_data['settings'] = user.settings
        output.append(user_data)
    return jsonify(output), 200

@users.route('/api/users/<public_id>', methods=['GET'])
@auth_required(minimum_role=UserRole.student)
def get_one_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'error': 'No user found'}), 404

    user_data = {}
    user_data['publicId'] = user.public_id
    user_data['groupId'] = user.group_id
    user_data['firstName'] = user.first_name
    user_data['lastName'] = user.last_name
    user_data['email'] = user.email
    user_data['registrationDate'] = user.created_at
    user_data['lastLogin'] = user.last_login
    user_data['role'] = user.role
    user_data['achievements'] = user.achievements
    user_data['settings'] = user.settings

    return jsonify(user_data), 200

@users.route('/api/users', methods=['POST'])
@auth_required(minimum_role=UserRole.teacher)
def add_user(current_user):
    data = request.get_json()
    email = data['email'].lower()

    if (role_permissions[data['role']] > role_permissions[current_user.role]):
        return jsonify({'error': 'You do not have required permission'}), 403

    if User.query.filter_by(email=email).first():
        return jsonify({'error': 'User already exists'}), 400

    if 'password' in data:
        hashed_password = generate_password_hash(data['password'], method='sha256')
    else:
        generated_password = generate_password()
        hashed_password = generate_password_hash(generated_password, method='sha256')

    new_user = User(
        public_id=str(uuid.uuid4()),
        email=email,
        password=hashed_password,
        role=data['role']
    )
    if 'groupId' in data:
        new_user.group_id = data['groupId']

    if 'notify' in data and data['notify'] and generate_password:
        send_password_email(email, email, generated_password)

    db.session.add(new_user)

    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.user_count += 1

    db.session.commit()

    return jsonify({'message': 'New user created', 'publicId': new_user.public_id}), 201, {'Location': '/users/{}'.format(new_user.public_id)}

@users.route('/api/users/<public_id>', methods=['PUT'])
@auth_required(minimum_role=UserRole.student)
def update_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'error': 'No user found'}), 404

    data = request.get_json()

    if 'groupId' in data:
        user.group_id = data['groupId']

    if (role_permissions[current_user.role] >= role_permissions['teacher'] and 'role' in data):
        if (role_permissions[data['role']] > role_permissions[current_user.role]):
            return jsonify({'error': 'You do not have required permission'}), 403

        user.role = data['role']
        if 'notify' in data and data['notify']:
            generated_password = generate_password()
            hashed_password = generate_password_hash(generated_password, method='sha256')
            user.password = hashed_password
            send_password_email(user.email, user.email, generated_password)
    else:
        if 'firstName' in data:
            user.first_name = data['firstName']
        if 'lastName' in data:
            user.last_name = data['lastName']
        if 'achievements' in data:
            user.achievements = data['achievements']

    db.session.commit()
    return jsonify({'message': 'User updated'}), 200

@users.route('/api/users/<public_id>', methods=['DELETE'])
@auth_required(minimum_role=UserRole.admin)
def delete_user(current_user, public_id):
    user = User.query.filter_by(public_id=public_id).first()

    if not user:
        return jsonify({'error': 'No user found'}), 404

    db.session.delete(user)

    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.user_count = application_statistics.user_count - 1

    db.session.commit()
    return jsonify({'message': 'The user has been deleted'}), 204
