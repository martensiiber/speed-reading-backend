# -*- coding: utf-8 -*-
import unittest
import uuid
from werkzeug.security import generate_password_hash
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import User, UserRole, ApplicationStatistics
from tests.utils import auth
import credentials
from tests.data import test_credentials

class TestUsers(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(User(
            public_id=str(uuid.uuid4()),
            email='first@test.com',
            first_name='First',
            last_name='Last',
            password=generate_password_hash('Password', method='sha256'),
            role=UserRole.student
        ))
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.user_count = 3
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_user_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user_data = {
            'email': 'second@test.com',
            'password': 'Password',
            'role': UserRole.student
        }
        response = self.client.post('/api/users', json=user_data, headers={'x-access-token': token})
        user = User.query.filter_by(id=6).first()
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'publicId': user.public_id,
            'message': 'New user created',
        })

    def test_add_user_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user_data = {
            'email': 'second@test.com',
            'password': 'Password',
            'role': UserRole.student
        }
        self.client.post('/api/users', json=user_data, headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.user_count, 4)

    def test_get_all_users_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        response = self.client.get('/api/users', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 5)
        found_user = [x for x in response.json if x['publicId'] == user.public_id][0]
        self.assertEqual(found_user, {
            'publicId': user.public_id,
            'groupId': None,
            'firstName': 'First',
            'lastName': 'Last',
            'email': 'first@test.com',
            'registrationDate': user.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'lastLogin': None,
            'role': UserRole.student,
            'achievements': None,
            'settings': None
        })

    def test_get_all_users_as_statistician_returns_response(self):
        token = auth(self.client, test_credentials.statistician['username'], test_credentials.statistician['password'])
        user = User.query.filter_by(email='first@test.com').first()
        anonymous_identifier = str(user.public_id[-3:]).rjust(7, '#')
        response = self.client.get('/api/users', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 5)
        found_user = [x for x in response.json if x['publicId'] == user.public_id][0]
        self.assertEqual(found_user, {
            'publicId': user.public_id,
            'groupId': None,
            'firstName': anonymous_identifier,
            'lastName': anonymous_identifier,
            'email': anonymous_identifier + '@gmail.com',
            'registrationDate': user.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'lastLogin': None,
            'role': UserRole.student,
            'achievements': None,
            'settings': None
        })

    def test_get_one_user_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        response = self.client.get('/api/users/' + user.public_id, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'publicId': user.public_id,
            'groupId': None,
            'firstName': 'First',
            'lastName': 'Last',
            'email': 'first@test.com',
            'registrationDate': user.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'lastLogin': None,
            'role': UserRole.student,
            'achievements': None,
            'settings': None
        })

    def test_get_unknown_user_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/users/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No user found'
        })

    def test_update_user_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        user_data = {
            'firstName': 'First name'
        }
        response = self.client.put('/api/users/' + user.public_id, json=user_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'User updated'
        })

    def test_update_unknown_user_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user_data = {
            'firstName': 'First name'
        }
        response = self.client.put('/api/users/0', json=user_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No user found'
        })

    def test_delete_user_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        response = self.client.delete('/api/users/' + user.public_id, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 204)

    def test_delete_unknown_user_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/users/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No user found'
        })

    def test_delete_user_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        self.client.delete('/api/users/' + user.public_id, headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.user_count, 2)

if __name__ == '__main__':
    unittest.main()
