from flask import Blueprint, jsonify, request
from api import db, utils
from api.models import UserRole, Abstraction
from api.decorators import auth_required

abstractions = Blueprint('abstractions', __name__)

@abstractions.route('/api/abstractions', methods=['POST'])
@auth_required(minimum_role=UserRole.teacher)
def update_abstraction(current_user):
    data = request.get_json()
    lemma = data['lemma'].lower()
    abstraction = data['abstraction']
    language = data['language']
    if abstraction not in [None, 1, 2, 3]:
        return jsonify({'error': 'Unknown abstraction'}), 400

    lemma_abstraction = Abstraction.query.filter_by(lemma=lemma).first()
    if lemma_abstraction is None:
        new_lemma_abstraction = Abstraction(
            lemma=lemma,
            abstraction_rating=abstraction,
            language=language,
            user_id=current_user.public_id
        )
        db.session.add(new_lemma_abstraction)
        db.session.commit()
        result = jsonify({'message': 'Abstraction added'}), 201
    else:
        lemma_abstraction.abstraction_rating = abstraction
        lemma_abstraction.user_id = current_user.public_id
        db.session.commit()
        result = jsonify({'message': 'Abstraction updated'}), 200
    utils.update_abstraction(lemma, abstraction, language)
    return result
