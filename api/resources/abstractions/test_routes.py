# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import Abstraction, User
from tests.utils import auth
import credentials

class TestAbstractions(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        demo_user = User.query.filter_by(email=credentials.demo['username']).first()
        db.session.add(Abstraction(
            lemma='test',
            abstraction_rating=1,
            language='estonian',
            user_id=demo_user.public_id
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_abstraction_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        abstraction_data = {
            'lemma': 'testimine',
            'abstraction': 2,
            'language': 'estonian'
        }
        response = self.client.post('/api/abstractions', json=abstraction_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Abstraction added'
        })

    def test_update_abstraction_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        abstraction_data = {
            'lemma': 'test',
            'abstraction': 2,
            'language': 'estonian'
        }
        response = self.client.post('/api/abstractions', json=abstraction_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Abstraction updated'
        })

    def test_update_unknown_abstraction_returns_error(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        abstraction_data = {
            'lemma': 'test',
            'abstraction': -1,
            'language': 'estonian'
        }
        response = self.client.post('/api/abstractions', json=abstraction_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Unknown abstraction'
        })

    def test_update_abstraction_updates_abstraction_and_user(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        abstraction_data = {
            'lemma': 'test',
            'abstraction': 2,
            'language': 'estonian'
        }
        self.client.post('/api/abstractions', json=abstraction_data, headers={'x-access-token': token})
        admin_user = User.query.filter_by(email=credentials.admin['username']).first()
        abstraction = Abstraction.query.filter_by(lemma='test').first()
        self.assertEqual(abstraction.abstraction_rating, 2)
        self.assertEqual(abstraction.user_id, admin_user.public_id)

if __name__ == '__main__':
    unittest.main()
