from flask import Blueprint, jsonify, request
from api import db
from api.models import AnalyzedText, UserRole
from api.decorators import auth_required

analyzed_texts = Blueprint('analyzed_texts', __name__)

@analyzed_texts.route('/api/analyzedTexts', methods=['GET'])
@auth_required()
def get_all_analyzed_texts(current_user):
    if current_user.role == UserRole.admin:
        analyzed_texts = AnalyzedText.query.all()
    else:
        analyzed_texts = AnalyzedText.query.filter_by(user_id=current_user.public_id).all()

    output = []
    for analyzed_text in analyzed_texts:
        analyzed_text_data = {}
        analyzed_text_data['id'] = analyzed_text.id
        analyzed_text_data['identifier'] = analyzed_text.identifier
        analyzed_text_data['language'] = analyzed_text.language
        analyzed_text_data['text'] = analyzed_text.text
        analyzed_text_data['analysis'] = analyzed_text.analysis

        output.append(analyzed_text_data)
    return jsonify(output), 200

@analyzed_texts.route('/api/analyzedTexts/<id>', methods=['GET'])
@auth_required()
def get_one_analyzed_text(current_user, id):
    analyzed_text = AnalyzedText.query.filter_by(id=id).first()

    if not analyzed_text:
        return jsonify({'error': 'No analyzed text found'}), 404

    analyzed_text_data = {}
    analyzed_text_data['id'] = analyzed_text.id
    analyzed_text_data['identifier'] = analyzed_text.identifier
    analyzed_text_data['language'] = analyzed_text.language
    analyzed_text_data['text'] = analyzed_text.text
    analyzed_text_data['analysis'] = analyzed_text.analysis

    return jsonify(analyzed_text_data), 200

@analyzed_texts.route('/api/analyzedTexts', methods=['POST'])
@auth_required()
def add_analyzed_text(current_user):
    data = request.get_json()

    new_analyzed_text = AnalyzedText(
        user_id=current_user.public_id,
        identifier=data['identifier'],
        language=data['language'],
        text=data['text'],
        analysis=data['analysis']
    )
    db.session.add(new_analyzed_text)
    db.session.commit()

    return jsonify({'message': 'Analyzed text added', 'id': new_analyzed_text.id}), 201, {'Location': '/analyzedTexts/{}'.format(new_analyzed_text.id)}

@analyzed_texts.route('/api/analyzedTexts/<id>', methods=['PUT'])
@auth_required()
def update_analyzed_text(current_user, id):
    analyzed_text = AnalyzedText.query.filter_by(id=id).first()

    if not analyzed_text:
        return jsonify({'error': 'No analyzed text found'}), 404

    data = request.get_json()
    analyzed_text.identifier = data['identifier']
    # analyzed_text.language = data['language']
    analyzed_text.text = data['text']
    analyzed_text.analysis = data['analysis']

    db.session.commit()
    return jsonify({'message': 'Analyzed text updated'}), 200

@analyzed_texts.route('/api/analyzedTexts/<id>', methods=['DELETE'])
@auth_required()
def delete_analyzed_text(current_user, id):
    analyzed_text = AnalyzedText.query.filter_by(id=id).first()

    if not analyzed_text:
        return jsonify({'error': 'No analyzed text found'}), 404

    db.session.delete(analyzed_text)
    db.session.commit()
    return jsonify({'message': 'The analyzed text has been deleted'}), 204
