# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import AnalyzedText, User
from tests.utils import auth
import credentials

class TestQuestions(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.add(AnalyzedText(
            user_id=user.public_id,
            identifier='Test',
            language='english',
            text='Analyzed text',
            analysis=[]
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_analyzed_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        analyzed_text_data = {
            'identifier': 'Test2',
            'language': 'english',
            'text': 'Some analyzed text',
            'analysis': []
        }
        response = self.client.post('/api/analyzedTexts', json=analyzed_text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 2,
            'message': 'Analyzed text added'
        })

    def test_get_one_analyzed_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/analyzedTexts/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 1,
            'identifier': 'Test',
            'language': 'english',
            'text': 'Analyzed text',
            'analysis': []
        })

    def test_get_unknown_analyzed_text_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/analyzedTexts/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No analyzed text found'
        })

    def test_get_analyzed_texts_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/analyzedTexts', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'identifier': 'Test',
            'language': 'english',
            'text': 'Analyzed text',
            'analysis': []
        }])

    def test_update_analyzed_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        analyzed_text_data = {
            'identifier': 'Test2',
            'language': 'english',
            'text': 'Some analyzed text',
            'analysis': []
        }
        response = self.client.put('/api/analyzedTexts/1', json=analyzed_text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Analyzed text updated'
        })

    def test_update_unknown_analyzed_text_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        analyzed_text_data = {
            'identifier': 'Test2',
            'language': 'english',
            'text': 'Some analyzed text',
            'analysis': []
        }
        response = self.client.put('/api/analyzedTexts/0', json=analyzed_text_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No analyzed text found'
        })

    def test_delete_analyzed_text_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/analyzedTexts/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 204)

    def test_delete_unknown_analyzed_text_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/analyzedTexts/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No analyzed text found'
        })

if __name__ == '__main__':
    unittest.main()
