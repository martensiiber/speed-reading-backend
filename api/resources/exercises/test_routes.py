# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ExerciseType
from tests.utils import auth
import credentials

class TestExercises(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_exercise_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        exercise_data = {
            'name': 'New help exercise',
            'type': ExerciseType.help
        }
        response = self.client.post('/api/exercises', json=exercise_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 10,
            'message': 'Exercise added'
        })

    def test_get_exercises_returns_response(self):
        response = self.client.get('/api/exercises')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'name': 'readingTest',
            'type': 'reading'
        }, {
            'id': 2,
            'name': 'readingAid',
            'type': 'reading'
        }, {
            'id': 3,
            'name': 'scrolling',
            'type': 'reading'
        }, {
            'id': 4,
            'name': 'disappearing',
            'type': 'reading'
        }, {
            'id': 5,
            'name': 'wordGroups',
            'type': 'reading'
        }, {
            'id': 6,
            'name': 'schulteTables',
            'type': 'help'
        }, {
            'id': 7,
            'name': 'concentration',
            'type': 'help'
        }, {
            'id': 8,
            'name': 'verticalReading',
            'type': 'reading'
        }, {
            'id': 9,
            'name': 'movingWordGroups',
            'type': 'reading'
        }])

if __name__ == '__main__':
    unittest.main()
