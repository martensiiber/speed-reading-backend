from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, Exercise
from api.decorators import auth_required

exercises = Blueprint('exercises', __name__)

@exercises.route('/api/exercises', methods=['GET'])
def get_exercises():
    exercises = Exercise.query.all()

    output = []
    for exercise in exercises:
        exercise_data = {}
        exercise_data['id'] = exercise.id
        exercise_data['name'] = exercise.name
        exercise_data['type'] = exercise.type

        output.append(exercise_data)
    return jsonify(output), 200

@exercises.route('/api/exercises', methods=['POST'])
@auth_required(minimum_role=UserRole.admin)
def add_exercise(current_user):
    data = request.get_json()
    new_exercise = Exercise(
        name=data['name'],
        type=data['type']
    )
    db.session.add(new_exercise)
    db.session.commit()
    return jsonify({'message': 'Exercise added', 'id': new_exercise.id}), 201, {'Location': '/exercises/{}'.format(new_exercise.id)}
