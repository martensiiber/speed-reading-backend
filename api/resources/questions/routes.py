from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, Question, ApplicationStatistics
from api.decorators import auth_required

questions = Blueprint('questions', __name__)

@questions.route('/api/questions', methods=['GET'])
def get_all_questions():
    reading_text_id = request.args.get('readingTextId')
    embed = request.args.get('embed')
    if reading_text_id:
        questions = Question.query.filter_by(reading_text_id=reading_text_id).all()
    else:
        questions = Question.query.all()

    output = []
    for question in questions:
        question_data = {}
        question_data['id'] = question.id
        question_data['readingTextId'] = question.reading_text_id
        question_data['category'] = question.category
        question_data['questionText'] = question.question_text
        answers = []
        for answer in question.answers:
            answer_data = {}
            answer_data['id'] = answer.id
            answer_data['questionId'] = answer.question_id
            answer_data['answerText'] = answer.answer_text
            if embed == 'answers.correct':
                answer_data['correct'] = answer.correct
            answers.append(answer_data)
        question_data['answers'] = answers
        output.append(question_data)

    return jsonify(output), 200

@questions.route('/api/questions/<id>', methods=['GET'])
def get_one_question(id):
    question = Question.query.filter_by(id=id).first()

    if not question:
        return jsonify({'error': 'No question found'}), 404

    question_data = {}
    question_data['id'] = question.id
    question_data['readingTextId'] = question.reading_text_id
    question_data['category'] = question.category
    question_data['questionText'] = question.question_text

    return jsonify(question_data), 200

@questions.route('/api/questions', methods=['POST'])
@auth_required(minimum_role=UserRole.editor)
def add_question(current_user):
    data = request.get_json()
    new_question = Question(
        reading_text_id=data['readingTextId'],
        category=data['category'],
        question_text=data['questionText']
    )
    db.session.add(new_question)
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.question_count += 1
    db.session.commit()
    return jsonify({'message': 'Question added', 'id': new_question.id}), 201, {'Location': '/questions/{}'.format(new_question.id)}

@questions.route('/api/questions/<id>', methods=['PUT'])
@auth_required(minimum_role=UserRole.editor)
def update_question(current_user, id):
    question = Question.query.filter_by(id=id).first()

    if not question:
        return jsonify({'error': 'No question found'}), 404

    data = request.get_json()
    question.reading_text_id = data['readingTextId']
    question.category = data['category']
    question.question_text = data['questionText']

    db.session.commit()
    return jsonify({'message': 'Question updated'}), 200

@questions.route('/api/questions/<id>', methods=['DELETE'])
@auth_required(minimum_role=UserRole.editor)
def delete_question(current_user, id):
    question = Question.query.filter_by(id=id).first()

    if not question:
        return jsonify({'error': 'No question found'}), 404

    db.session.delete(question)
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.question_count = application_statistics.question_count - 1
    db.session.commit()
    return jsonify({'message': 'The question has been deleted'}), 204
