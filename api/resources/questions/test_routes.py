# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, Question, QuestionCategory, ApplicationStatistics
from tests.utils import auth
from tests.data.resources import reading_text_data_est
import credentials

class TestQuestions(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.add(Question(
            reading_text_id=1,
            category=QuestionCategory.question,
            question_text='Test question'
        ))
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.question_count = 1
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_question_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        question_data = {
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Test question'
        }
        response = self.client.post('/api/questions', json=question_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 2,
            'message': 'Question added'
        })

    def test_add_question_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        question_data = {
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Test question'
        }
        self.client.post('/api/questions', json=question_data, headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.question_count, 2)

    def test_get_questions_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/questions', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Test question',
            'answers': []
        }])

    def test_get_one_question_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/questions/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 1,
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Test question'
        })

    def test_update_question_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        question_data = {
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Updated test question'
        }
        response = self.client.put('/api/questions/1', json=question_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Question updated'
        })

    def test_update_unknown_question_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        question_data = {
            'readingTextId': 1,
            'category': 'question',
            'questionText': 'Updated test question'
        }
        response = self.client.put('/api/questions/0', json=question_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No question found'
        })

    def test_delete_question_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/questions/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 204)

    def test_delete_unknown_question_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/questions/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No question found'
        })

    def test_delete_question_updates_application_statistics(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        self.client.delete('/api/questions/1', headers={'x-access-token': token})
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.question_count, 0)

if __name__ == '__main__':
    unittest.main()
