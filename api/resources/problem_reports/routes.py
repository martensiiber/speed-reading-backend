from flask import Blueprint, jsonify, request
from base64 import b64decode
import uuid
from api import db
from api.models import UserRole, ProblemReport
from api.decorators import auth_required
from api.resources.utility.utils import send_problem_report_email

problem_reports = Blueprint('problem_reports', __name__)

@problem_reports.route('/api/problemReports', methods=['GET'])
@auth_required(minimum_role=UserRole.teacher)
def get_all_problem_reports(current_user):
    problem_reports = ProblemReport.query.all()

    output = []
    for problem_report in problem_reports:
        problem_report_data = {}
        problem_report_data['id'] = problem_report.id
        problem_report_data['date'] = problem_report.created_at
        problem_report_data['resolved'] = problem_report.resolved
        problem_report_data['type'] = problem_report.type
        problem_report_data['userId'] = problem_report.user_id
        problem_report_data['textTitle'] = problem_report.text_title
        problem_report_data['description'] = problem_report.description
        problem_report_data['screenshotFilename'] = problem_report.screenshot_filename
        output.append(problem_report_data)
    return jsonify(output), 200

@problem_reports.route('/api/problemReports', methods=['POST'])
def add_problem_report():
    data = request.get_json()
    new_problem_report = ProblemReport(
        type=data['type'],
        description=data['description']
    )
    if 'userId' in data:
        new_problem_report.user_id = data['userId']
    if 'textTitle' in data and data['textTitle'] is not None:
        new_problem_report.text_title = data['textTitle']
    if 'screenshot' in data and data['screenshot'] is not None:
        imageData = b64decode(data['screenshot'])
        # Saving screenshot
        filename = str(uuid.uuid4()) + '.png'
        with open('api/static/submitted_screenshots/' + filename, 'wb') as fh:
            fh.write(imageData)
        new_problem_report.screenshot_filename = filename
    db.session.add(new_problem_report)
    db.session.commit()

    send_problem_report_email(new_problem_report.text_title, new_problem_report.description)

    return jsonify({'message': 'Problem report added'}), 201, {'Location': '/problemReports/{}'.format(new_problem_report.id)}

@problem_reports.route('/api/problemReports/<id>', methods=['PATCH'])
def patch_problem_report(id):

    problem_report = ProblemReport.query.filter_by(id=id).first()

    if not problem_report:
        return jsonify({'error': 'No problem report found'}), 404

    data = request.get_json()

    if 'resolved' not in data:
        return jsonify({'error': 'Nothing to be patched'}), 400

    problem_report.resolved = data['resolved']

    db.session.commit()
    return jsonify({'message': 'Problem report patched'}), 200
