# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ProblemReport, ProblemType
from tests.utils import auth
import credentials

class TestProblemReports(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(ProblemReport(
            type=ProblemType.other,
            description='Description'
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_problem_report_returns_response(self):
        problem_report_data = {
            'type': 'other',
            'description': 'Description'
        }
        response = self.client.post('/api/problemReports', json=problem_report_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Problem report added'
        })

    def test_get_problem_reports_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/problemReports', headers={'x-access-token': token})
        problem_report = ProblemReport.query.filter_by(id=1).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'type': 'other',
            'textTitle': None,
            'date': problem_report.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'userId': None,
            'description': 'Description',
            'screenshotFilename': None,
            'resolved': False
        }])

    def test_patch_problem_report_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        problem_report_data = {
            'resolved': True
        }
        response = self.client.patch('/api/problemReports/1', json=problem_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Problem report patched'
        })

    def test_patch_unknown_problem_report_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        problem_report_data = {
            'resolved': True
        }
        response = self.client.patch('/api/problemReports/0', json=problem_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No problem report found'
        })

    def test_patch_nothing_on_problem_report_returns_error(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        problem_report_data = {}
        response = self.client.patch('/api/problemReports/1', json=problem_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Nothing to be patched'
        })

if __name__ == '__main__':
    unittest.main()
