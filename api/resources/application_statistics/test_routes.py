# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config

class TestApplicationStatistics(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_get_application_statistics_returns_response(self):
        response = self.client.get('/api/applicationStatistics')
        self.assertEqual(response.json, {
            'exerciseAttemptCount': 0,
            'textCount': 0,
            'questionCount': 0,
            'feedbackCount': 0,
            'userCount': 2,
            'readingExerciseTime': 0,
            'helpExerciseTime': 0,
            'testTime': 0,
            'totalTime': 0
        })

if __name__ == '__main__':
    unittest.main()
