
from flask import Blueprint, jsonify
from api.models import ApplicationStatistics

application_statistics = Blueprint('application_statistics', __name__)

@application_statistics.route('/api/applicationStatistics', methods=['GET'])
def get_application_statistics():
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()

    statistics = {}
    statistics['exerciseAttemptCount'] = application_statistics.exercise_attempt_count
    statistics['textCount'] = application_statistics.reading_text_count
    statistics['questionCount'] = application_statistics.question_count
    statistics['feedbackCount'] = application_statistics.feedback_count
    statistics['userCount'] = application_statistics.user_count

    reading_exercise_time = application_statistics.reading_exercise_time
    help_exercise_time = application_statistics.help_exercise_time
    test_time = application_statistics.test_time
    statistics['readingExerciseTime'] = reading_exercise_time
    statistics['helpExerciseTime'] = help_exercise_time
    statistics['testTime'] = test_time
    statistics['totalTime'] = reading_exercise_time + help_exercise_time + test_time

    return jsonify(statistics), 200
