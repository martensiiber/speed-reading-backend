# -*- coding: utf-8 -*-
import unittest
import uuid
from werkzeug.security import generate_password_hash
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import User, UserRole
from tests.utils import auth
import credentials

class TestAchievements(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(User(
            public_id=str(uuid.uuid4()),
            email='first@test.com',
            first_name='First',
            last_name='Last',
            password=generate_password_hash('Password', method='sha256'),
            role=UserRole.student,
            achievements={
                'unique': {},
                'daily': {},
                'weekly': {},
                'monthly': {},
                'progress': {}
            }
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_get_all_achievements_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        user = User.query.filter_by(email='first@test.com').first()
        response = self.client.get('/api/achievements', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 5)
        found_achievements = [x for x in response.json if x['publicId'] == user.public_id][0]
        self.assertEqual(found_achievements, {
            'publicId': user.public_id,
            'groupId': None,
            'achievements': {
                'unique': {},
                'daily': {},
                'weekly': {},
                'monthly': {},
                'progress': {}
            }
        })

if __name__ == '__main__':
    unittest.main()
