from flask import Blueprint, jsonify
from api.models import User
from api.decorators import auth_required

achievements = Blueprint('achievements', __name__)

@achievements.route('/api/achievements', methods=['GET'])
@auth_required()
def get_all_achievements(current_user):
    users = User.query.order_by(User.created_at.desc()).all()

    output = []
    for user in users:
        achievement_data = {}
        achievement_data['publicId'] = user.public_id
        achievement_data['groupId'] = user.group_id
        achievement_data['achievements'] = user.achievements
        output.append(achievement_data)
    return jsonify(output), 200
