from api import db, utils
from api.models import (
    User,
    TestAttempt,
    ExerciseAttempt,
    Exercise,
    ApplicationStatistics,
    ReadingText,
    Question,
    Feedback,
    UserTextRating,
    UserTestRating,
)

# Temporary application statistics calculation
def recalculate_application_statistics():  # pragma: no cover
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.exercise_attempt_count = ExerciseAttempt.query.count()
    application_statistics.reading_text_count = ReadingText.query.count()
    application_statistics.question_count = Question.query.count()
    application_statistics.feedback_count = Feedback.query.count()
    application_statistics.user_count = User.query.count()

    reading_exercise_time = help_exercise_time = test_time = 0

    exercise_attempts = ExerciseAttempt.query.all()
    for exercise_attempt in exercise_attempts:
        if exercise_attempt.result is not None:
            elapsed_time = exercise_attempt.result["elapsedTime"]
            if exercise_attempt.exercise.type == "reading":
                reading_exercise_time += elapsed_time
            elif exercise_attempt.exercise.type == "help":
                help_exercise_time += elapsed_time
        if exercise_attempt.test is not None:
            if exercise_attempt.test.result is not None:
                test_time += exercise_attempt.test.result["elapsedTime"]

    application_statistics.reading_exercise_time = reading_exercise_time
    application_statistics.help_exercise_time = help_exercise_time
    application_statistics.test_time = test_time

    db.session.commit()


# Temporary text statistics calculation
def analyze_reading_texts():  # pragma: no cover
    reading_texts = ReadingText.query.all()
    for reading_text in reading_texts:
        analysis = utils.analyze(reading_text.plain_text, reading_text.language)

        statistics = {}
        statistics["characterCount"] = analysis["characterCount"]
        statistics["wordCount"] = analysis["wordCount"]
        statistics["sentenceCount"] = analysis["sentenceCount"]
        statistics["wordLengthClassRating"] = analysis["wordLengthClassRating"]
        statistics["sentenceLengthClassRating"] = analysis["sentenceLengthClassRating"]

        (interestingness_rating_count, average_interestingness_rating) = (
            db.session.query(
                db.func.count(UserTextRating.id),
                db.func.avg(UserTextRating.interestingness_rating),
            )
            .filter(
                UserTextRating.reading_text_id == reading_text.id,
            )
            .first()
        )
        statistics["averageInterestingnessRating"] = (
            float(average_interestingness_rating)
            if average_interestingness_rating is not None
            else None
        )
        statistics["interestingnessRatingCount"] = interestingness_rating_count

        (test_difficulty_rating_count, average_test_difficulty_rating) = (
            db.session.query(
                db.func.count(UserTestRating.id),
                db.func.avg(UserTestRating.difficulty_rating),
            )
            .filter(
                UserTestRating.reading_text_id == reading_text.id,
            )
            .first()
        )

        statistics["averageTestDifficultyRating"] = (
            float(average_test_difficulty_rating)
            if average_test_difficulty_rating is not None
            else None
        )
        statistics["testDifficultyRatingCount"] = test_difficulty_rating_count

        statistics["totalReadingAttemptCount"] = ExerciseAttempt.query.filter(
            ExerciseAttempt.reading_text_id == reading_text.id,
        ).count()
        reading_text.statistics = statistics
        db.session.commit()


# Temporary JSON attributes converter
def convert_json_attributes():  # pragma: no cover
    exercise_attempts = ExerciseAttempt.query.all()
    for exercise_attempt in exercise_attempts:
        if exercise_attempt.result is not None:
            # wpm -> wordsPerMinute
            if "wpm" in exercise_attempt.result is not None:
                # print(exercise_attempt.result['wpm'])
                temp = exercise_attempt.result["wpm"]
                exercise_attempt.result["wordsPerMinute"] = temp
                del exercise_attempt.result["wpm"]
            # cps -> charactersPerMinute
            if "cps" in exercise_attempt.result is not None:
                # print(exercise_attempt.result['cps'])
                temp = exercise_attempt.result["cps"]
                exercise_attempt.result["charactersPerSecond"] = temp
                del exercise_attempt.result["cps"]
            if "spm" in exercise_attempt.result is not None:
                # print(exercise_attempt.result['spm'])
                temp = exercise_attempt.result["spm"]
                exercise_attempt.result["symbolsPerMinute"] = temp
                del exercise_attempt.result["spm"]
            if "symbolsPerSecond" in exercise_attempt.result is not None:
                # print(exercise_attempt.result['symbolsPerSecond'])
                temp = exercise_attempt.result["symbolsPerSecond"]
                exercise_attempt.result["symbolsPerMinute"] = temp
                del exercise_attempt.result["symbolsPerSecond"]
        # 25 -> 5x5
        if exercise_attempt.settings is not None:
            if "tableDimensions" in exercise_attempt.settings["exerciseOptions"]:
                if (
                    exercise_attempt.settings["exerciseOptions"]["tableDimensions"]
                    == 25
                ):
                    # print(exercise_attempt.settings['exerciseOptions']['tableDimensions'])
                    exercise_attempt.settings["exerciseOptions"][
                        "tableDimensions"
                    ] = "5x5"
                    exercise_attempt.settings[
                        "exerciseOptions"
                    ] = exercise_attempt.settings["exerciseOptions"]
            if "wpm" in exercise_attempt.settings["speedOptions"]:
                # print(exercise_attempt.settings['speedOptions']['wpm'])
                temp = exercise_attempt.settings["speedOptions"]["wpm"]
                exercise_attempt.settings["speedOptions"]["wordsPerMinute"] = temp
                del exercise_attempt.settings["speedOptions"]["wpm"]
                exercise_attempt.settings["speedOptions"] = exercise_attempt.settings[
                    "speedOptions"
                ]
        db.session.commit()

    test_attempts = TestAttempt.query.all()
    for test_attempt in test_attempts:
        if test_attempt.result is not None:
            # cpm -> comprehensionPerMinute
            if "cpm" in test_attempt.result:
                # print(test_attempt.result['cpm'])
                temp = test_attempt.result["cpm"]
                test_attempt.result["comprehensionPerMinute"] = temp
                del test_attempt.result["cpm"]
                db.session.commit()


# Temporary Concentration exercise calculation
def calculate_concentration():  # pragma: no cover
    exercise_attempts = ExerciseAttempt.query.filter_by(
        exercise=Exercise.query.filter_by(name="concentration").first()
    )
    for exercise_attempt in exercise_attempts:
        if exercise_attempt.result is None:
            continue
        if "symbolGroupCount" in exercise_attempt.settings["exerciseOptions"]:
            group_count = exercise_attempt.settings["exerciseOptions"][
                "symbolGroupCount"
            ]
            symbol_count = exercise_attempt.settings["exerciseOptions"]["symbolCount"]
            correct = exercise_attempt.result["correct"]
            elapsed_seconds = exercise_attempt.result["elapsedTime"]
            ms_per_symbol_group = round(elapsed_seconds / group_count)
            ms_per_symbol = round(elapsed_seconds / (2 * symbol_count * group_count))
            exercise_attempt.result["msPerSymbolGroup"] = ms_per_symbol_group
            exercise_attempt.result["msPerSymbol"] = ms_per_symbol
            db.session.commit()
            # print(str(ms_per_symbol_group) + 'ms/group')
            # print(str(ms_per_symbol) + 'ms/symbol')
        else:
            print("Added symbolGroupCount")
            exercise_options = exercise_attempt.settings["exerciseOptions"]
            exercise_options["symbolGroupCount"] = 20
            exercise_attempt.settings["exerciseOptions"] = exercise_options
            db.session.commit()


def calculate_exercise_attempt_statistics():
    attempts = ExerciseAttempt.query.order_by(ExerciseAttempt.created_at.asc()).all()
    for attempt in attempts:
        if attempt.reading_text_id:
            previous_attempts = [
                previous
                for previous in attempts
                if previous.user_id == attempt.user_id
                and previous.reading_text_id == attempt.reading_text_id
                and previous.start_time < attempt.start_time
            ]
            text_reading_attempt_count = len(previous_attempts) + 1
            if attempt.statistics is None:
                attempt.statistics = {}
            attempt.statistics['textReadingAttemptCount'] = text_reading_attempt_count
    db.session.commit()
