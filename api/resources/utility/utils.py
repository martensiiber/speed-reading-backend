from flask import current_app
from flask_mail import Message
from api import db, mail, utils
from api.models import TestAttempt, ExerciseAttempt, TestQuestionAnswer, Answer, TestBlankAnswer, BlankAnswerCategory
import credentials
from collections import Mapping

role_permissions = dict(
    guest=0,
    student=1,
    statistician=2,
    editor=3,
    teacher=4,
    developer=5,
    admin=6
)

def generate_password(length=8):
    import string
    import random
    characters = string.ascii_letters + string.digits
    return ''.join(random.choice(characters) for _ in range(length))

def merge_dict(dict1, dict2):
    for key, value in dict2.items():
        if isinstance(dict1.get(key), dict) and isinstance(value, Mapping):
            dict1[key] = merge_dict(dict1[key], dict2[key])
        else:
            dict1[key] = value
    return dict1

def send_password_email(recipient, email, password):
    message = Message('Kiirlugemise tarkvara kasutajakonto', recipients=[recipient])
    message.html = """
        Kasutajanimi: {email}<br>
        Parool: <b>{password}</b><br>
        Rakendus: <a href=https://kiirlugemine.keeleressursid.ee>https://kiirlugemine.keeleressursid.ee</a> <br><br>
        Genereeritud parooli vahetamiseks tuleb peale sisselogimist vajutada paremal servas oleval punasel kasutajakonto ikoonil ja valida 'Vaheta parool'.
    """.format(email=email, password=password)
    mail.send(message)

def send_bug_report_email(description):
    if not current_app.debug:
        message = Message('Kiirlugemise tarkvara veateade', recipients=credentials.bug_report['emails'])
        message.html = """
            <b>Veakirjeldus: </b>{description}<br>
        """.format(description=description)
        mail.send(message)

def send_problem_report_email(text_title, description):
    if not current_app.debug:
        message = Message('Kiirlugemise tarkvara probleemi teade', recipients=credentials.problem_report['emails'])
        message.html = ""
        if text_title is not None:
            message.html += """
                <b>Teksti pealkiri: </b>{text_title}<br>
            """.format(text_title=text_title)
        message.html += """
            <b>Probleemi kirjeldus: </b>{description}<br>
        """.format(description=description)
        mail.send(message)

def evaluate_blank_answer(correct, answer, language):
    if answer is None:
        return BlankAnswerCategory.unanswered
    if correct == answer:
        return BlankAnswerCategory.correct
    if utils.get_levenshtein_distance(correct, answer) <= 2:
        return BlankAnswerCategory.misspelled
    is_synonym = utils.is_synonym(correct, answer, language)
    if is_synonym:
        return BlankAnswerCategory.synonym
    return BlankAnswerCategory.incorrect

def evaluate_test_attempt(test_attempt):
    exercise_attempt = ExerciseAttempt.query.filter_by(id=test_attempt.exercise_attempt_id).first()
    total = correct = incorrect = unanswered = test_result = comprehension_result = 0
    question_answers = TestQuestionAnswer.query.filter_by(test_attempt_id=test_attempt.id).all()
    blank_answers = TestBlankAnswer.query.filter_by(test_attempt_id=test_attempt.id).all()
    if len(question_answers) > 0:
        question_count = len(question_answers)
        question_weight = 1 / question_count if question_count > 0 else 0
        for question_answer in question_answers:
            if question_answer.answer_id is not None:
                answer = Answer.query.get(question_answer.answer_id)
                answer_count = Answer.query.filter_by(question_id=question_answer.question_id).count()
                if answer.correct:
                    correct += 1
                    comprehension_result += question_weight
                else:
                    incorrect += 1
                    comprehension_result -= question_weight / (answer_count - 1) if answer_count > 1 else question_weight
            else:
                unanswered += 1
            total += 1
        test_result = correct / total if total > 0 else 0
        comprehension_result = max(comprehension_result, 0)
    elif len(blank_answers) > 0:
        for blank_answer in blank_answers:
            if blank_answer.auto_evaluation != BlankAnswerCategory.unanswered:
                if blank_answer.auto_evaluation != BlankAnswerCategory.incorrect or blank_answer.user_evaluation is not None and blank_answer.user_evaluation != BlankAnswerCategory.incorrect:
                    correct += 1
                else:
                    incorrect += 1
            else:
                unanswered += 1
            total += 1
        test_result = correct / total if total > 0 else 0
        comprehension_result = test_result
    result_data = {}
    result_data['total'] = total
    result_data['correct'] = correct
    result_data['incorrect'] = incorrect
    result_data['unanswered'] = unanswered
    result_data['testResult'] = test_result
    result_data['comprehensionResult'] = comprehension_result
    result_data['comprehensionPerMinute'] = exercise_attempt.result['wordsPerMinute'] * comprehension_result
    return result_data

def reevaluate_test_attempts(): # pragma: no cover
    test_attempts = TestAttempt.query.all()

    for test_attempt in test_attempts:
        exercise_attempt = ExerciseAttempt.query.filter_by(id=test_attempt.exercise_attempt_id).first()
        reading_text_id = exercise_attempt.reading_text_id
        if reading_text_id is not None and test_attempt.result is not None:
            total = correct = incorrect = unanswered = comprehension_result = 0
            question_answers = TestQuestionAnswer.query.filter_by(test_attempt_id=test_attempt.id)
            question_count = question_answers.count()
            question_weight = 1 / question_count if question_count > 0 else 0
            for question_answer in question_answers:
                if question_answer.answer_id is not None:
                    answer = Answer.query.get(question_answer.answer_id)
                    answer_count = Answer.query.filter_by(question_id=question_answer.question_id).count()
                    if answer.correct:
                        correct += 1
                        comprehension_result += question_weight
                    else:
                        incorrect += 1
                        comprehension_result -= question_weight / (answer_count - 1) if answer_count > 1 else question_weight
                else:
                    unanswered += 1
                total += 1

            result_data = test_attempt.result
            test_result = correct / total if total > 0 else 0
            comprehension_result = max(comprehension_result, 0)
            # result_data['total'] = total
            # result_data['correct'] = correct
            # result_data['incorrect'] = incorrect
            # result_data['unanswered'] = unanswered
            # result_data['testResult'] = test_result
            result_data['comprehensionResult'] = max(comprehension_result, 0)
            result_data['comprehensionPerMinute'] = exercise_attempt.result['wordsPerMinute'] * comprehension_result
            test_attempt.result = result_data
            db.session.commit()
        elif reading_text_id is None and test_attempt.result is not None:
            result_data = test_attempt.result
            test_result = result_data['correct'] / result_data['total'] if result_data['total'] > 0 else 0
            comprehension_result = test_result
            result_data['testResult'] = test_result
            result_data['comprehensionResult'] = comprehension_result
            result_data['comprehensionPerMinute'] = exercise_attempt.result['wordsPerMinute'] * comprehension_result
            test_attempt.result = result_data
            db.session.commit()
