# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import BlankAnswerCategory
from api.resources.utility.utils import evaluate_blank_answer

class TestUtils(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_evaluate_blank_answer_evaluates_unanswered(self):
        result = evaluate_blank_answer('test', None, 'estonian')
        self.assertEqual(result, BlankAnswerCategory.unanswered)

    def test_evaluate_blank_answer_evaluates_correct(self):
        result = evaluate_blank_answer('test', 'test', 'estonian')
        self.assertEqual(result, BlankAnswerCategory.correct)

    def test_evaluate_blank_answer_evaluates_misspelled(self):
        result = evaluate_blank_answer('test', 'tets', 'estonian')
        self.assertEqual(result, BlankAnswerCategory.misspelled)

    def test_evaluate_blank_answer_evaluates_incorrect(self):
        result = evaluate_blank_answer('test', '', 'estonian')
        self.assertEqual(result, BlankAnswerCategory.incorrect)

if __name__ == '__main__':
    unittest.main()
