# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import User, ExerciseAttempt, TestAttempt, TestBlankAnswer, BlankAnswerCategory, TextLanguage
from tests.utils import auth
import credentials

class TestExerciseTestBlankAnswers(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.commit()
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=1,
            modification='default',
            start_time=datetime.now(),
            result={
                'wordsPerMinute': 200
            }
        ))
        db.session.commit()
        db.session.add(TestAttempt(
            exercise_attempt_id=1,
            start_time=datetime.now()
        ))
        db.session.add(TestAttempt(
            exercise_attempt_id=1,
            start_time=datetime.now()
        ))
        db.session.commit()
        db.session.add(TestBlankAnswer(
            test_attempt_id=1,
            language=TextLanguage.estonian,
            blank_exercise=['See on ', 'BLANK', '.'],
            correct='lünkülesanne',
            answer='lünkülesanne',
            auto_evaluation=BlankAnswerCategory.correct
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_one_blank_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'testAttemptId': 2,
            'language': 'estonian',
            'blankExercise': ['See on ', 'BLANK', '.'],
            'correct': 'lünkülesanne',
            'answer': 'lünkülesanne'
        }
        response = self.client.post('/api/testBlankAnswers', json=test_blank_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Test blank answer added'
        })

    def test_add_multiple_blank_answers_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = [{
            'testAttemptId': 2,
            'language': 'estonian',
            'blankExercise': ['See on ', 'BLANK', '.'],
            'correct': 'lünkülesanne',
            'answer': 'lünkülesanne'
        }]
        response = self.client.post('/api/testBlankAnswers', json=test_blank_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Test blank answers added'
        })

    def test_add_incorrect_blank_answer_updates_auto_evaluation(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'testAttemptId': 2,
            'language': 'estonian',
            'blankExercise': ['See on ', 'BLANK', '.'],
            'correct': 'lünkülesanne',
            'answer': 'ülesanne'
        }
        self.client.post('/api/testBlankAnswers', json=test_blank_answer_data, headers={'x-access-token': token})
        test_blank_answer = TestBlankAnswer.query.filter_by(id=2).first()
        self.assertEqual(test_blank_answer.auto_evaluation, BlankAnswerCategory.incorrect)

    def test_add_mispelled_blank_answer_updates_auto_evaluation(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'testAttemptId': 2,
            'language': 'estonian',
            'blankExercise': ['See on ', 'BLANK', '.'],
            'correct': 'lünkülesanne',
            'answer': 'lünküleasnne'
        }
        self.client.post('/api/testBlankAnswers', json=test_blank_answer_data, headers={'x-access-token': token})
        test_blank_answer = TestBlankAnswer.query.filter_by(id=2).first()
        self.assertEqual(test_blank_answer.auto_evaluation, BlankAnswerCategory.misspelled)

    def test_patch_blank_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'userEvaluation': 'synonym'
        }
        response = self.client.patch('/api/testBlankAnswers/1', json=test_blank_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Test blank answer patched'
        })

    def test_patch_unknown_blank_answer_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'userEvaluation': 'synonym'
        }
        response = self.client.patch('/api/testBlankAnswers/0', json=test_blank_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No test blank answer found'
        })

    def test_patch_blank_answer_updates_answer(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_blank_answer_data = {
            'userEvaluation': 'synonym'
        }
        self.client.patch('/api/testBlankAnswers/1', json=test_blank_answer_data, headers={'x-access-token': token})
        test_blank_answer =  TestBlankAnswer.query.filter_by(id=1).first()
        self.assertEqual(test_blank_answer.user_evaluation, BlankAnswerCategory.synonym)

    def test_get_test_blank_answers_returns_response(self):
        response = self.client.get('/api/testBlankAnswers?testAttemptId=1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'blankExercise': ['See on ', 'BLANK', '.'],
            'language': 'estonian',
            'correct': 'lünkülesanne',
            'answer': 'lünkülesanne',
            'autoEvaluation': 'correct',
            'userEvaluation': None
        }])

if __name__ == '__main__':
    unittest.main()
