from flask import Blueprint, jsonify, request
from api import db
from api.models import TestBlankAnswer
from api.resources.utility.utils import evaluate_blank_answer

test_blank_answers = Blueprint('test_blank_answers', __name__)

@test_blank_answers.route('/api/testBlankAnswers', methods=['POST'])
def add_test_question_answer():
    data = request.get_json()
    if isinstance(data, list):
        # Many
        for blank_answer in data:
            auto_evaluation = evaluate_blank_answer(blank_answer['correct'], blank_answer['answer'], blank_answer['language'])
            new_blank_answer = TestBlankAnswer(
                test_attempt_id=blank_answer['testAttemptId'],
                language=blank_answer['language'],
                blank_exercise=blank_answer['blankExercise'],
                correct=blank_answer['correct'],
                answer=blank_answer['answer'],
                auto_evaluation=auto_evaluation
            )
            db.session.add(new_blank_answer)
        db.session.commit()
        result = jsonify({'message': 'Test blank answers added'}), 201
    else:
        # Single
        blank_answer = data
        auto_evaluation = evaluate_blank_answer(blank_answer['correct'], blank_answer['answer'], blank_answer['language'])
        new_blank_answer = TestBlankAnswer(
            test_attempt_id=blank_answer['testAttemptId'],
            language=blank_answer['language'],
            blank_exercise=blank_answer['blankExercise'],
            correct=blank_answer['correct'],
            answer=blank_answer['answer'],
            auto_evaluation=auto_evaluation
        )
        db.session.add(new_blank_answer)
        db.session.commit()
        result = jsonify({'message': 'Test blank answer added'}), 201

    return result

@test_blank_answers.route('/api/testBlankAnswers/<id>', methods=['PATCH'])
def patch_test_blank_answers(id):
    test_blank_answer = TestBlankAnswer.query.filter_by(id=id).first()

    if not test_blank_answer:
        return jsonify({'error': 'No test blank answer found'}), 404

    data = request.get_json()
    test_blank_answer.user_evaluation = data['userEvaluation']
    db.session.commit()

    return jsonify({'message': 'Test blank answer patched'}), 200

@test_blank_answers.route('/api/testBlankAnswers', methods=['GET'])
def get_test_blank_answers():
    test_attempt_id = request.args.get('testAttemptId')

    test_blank_answers = TestBlankAnswer.query.filter_by(test_attempt_id=test_attempt_id).all()
    output = []
    for test_blank_answer in test_blank_answers:
        blank_answer_data = {}
        blank_answer_data['id'] = test_blank_answer.id
        blank_answer_data['language'] = test_blank_answer.language
        blank_answer_data['blankExercise'] = test_blank_answer.blank_exercise
        blank_answer_data['correct'] = test_blank_answer.correct
        blank_answer_data['answer'] = test_blank_answer.answer
        blank_answer_data['autoEvaluation'] = test_blank_answer.auto_evaluation
        blank_answer_data['userEvaluation'] = test_blank_answer.user_evaluation
        output.append(blank_answer_data)

    return jsonify(output), 200
