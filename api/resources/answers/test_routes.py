# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, Question, QuestionCategory, Answer
from tests.utils import auth
from tests.data.resources import reading_text_data_est
import credentials

class TestAnswers(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.add(Question(
            reading_text_id=1,
            category=QuestionCategory.question,
            question_text='Test question'
        ))
        db.session.add(Answer(
            question_id=1,
            answer_text='Test answer',
            correct=True
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        answer_data = {
            'questionId': 1,
            'answerText': 'Test answer',
            'correct': False
        }
        response = self.client.post('/api/answers', json=answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 2,
            'message': 'Answer added'
        })

    def test_get_one_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 1,
            'questionId': 1,
            'answerText': 'Test answer'
        })

    def test_get_one_answer_embed_correct_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers/1?embed=correct', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 1,
            'questionId': 1,
            'answerText': 'Test answer',
            'correct': True
        })

    def test_get_unknown_answer_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No answer found'
        })

    def test_get_all_answers_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'questionId': 1,
            'answerText': 'Test answer'
        }])

    def test_get_question_answers_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers?questionId=1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'questionId': 1,
            'answerText': 'Test answer'
        }])

    def test_get_question_answers_embed_correct_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/answers?questionId=1&embed=correct', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'questionId': 1,
            'answerText': 'Test answer',
            'correct': True
        }])

    def test_update_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        answer_data = {
            'questionId': 1,
            'answerText': 'Test answer',
            'correct': False
        }
        response = self.client.put('/api/answers/1', json=answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Answer updated'
        })

    def test_update_unknown_answer_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        answer_data = {
            'questionId': 1,
            'answerText': 'Test answer',
            'correct': False
        }
        response = self.client.put('/api/answers/0', json=answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No answer found'
        })

    def test_delete_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/answers/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 204)

    def test_delete_unknown_answer_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/answers/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No answer found'
        })

if __name__ == '__main__':
    unittest.main()
