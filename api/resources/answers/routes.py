from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, Answer
from api.decorators import auth_required

answers = Blueprint('answers', __name__)

@answers.route('/api/answers', methods=['GET'])
def get_all_answers():
    question_id = request.args.get('questionId')
    embed = request.args.get('embed')
    if question_id:
        answers = Answer.query.filter_by(question_id=question_id).all()
    else:
        answers = Answer.query.all()

    output = []
    for answer in answers:
        answer_data = {}
        answer_data['id'] = answer.id
        answer_data['questionId'] = answer.question_id
        answer_data['answerText'] = answer.answer_text
        if embed == 'correct':
            answer_data['correct'] = answer.correct
        output.append(answer_data)

    return jsonify(output), 200

@answers.route('/api/answers/<id>', methods=['GET'])
def get_one_answer(id):
    answer = Answer.query.filter_by(id=id).first()
    embed = request.args.get('embed')

    if not answer:
        return jsonify({'error': 'No answer found'}), 404

    answer_data = {}
    answer_data['id'] = answer.id
    answer_data['questionId'] = answer.question_id
    answer_data['answerText'] = answer.answer_text
    if embed == 'correct':
        answer_data['correct'] = answer.correct

    return jsonify(answer_data), 200

@answers.route('/api/answers', methods=['POST'])
@auth_required(minimum_role=UserRole.editor)
def add_answer(current_user):
    data = request.get_json()
    new_answer = Answer(
        question_id=data['questionId'],
        answer_text=data['answerText'],
        correct=data['correct']
    )
    db.session.add(new_answer)
    db.session.commit()
    return jsonify({'message': 'Answer added', 'id': new_answer.id}), 201, {'Location': '/answers/{}'.format(new_answer.id)}

@answers.route('/api/answers/<id>', methods=['PUT'])
@auth_required(minimum_role=UserRole.editor)
def update_answer(current_user, id):
    answer = Answer.query.filter_by(id=id).first()

    if not answer:
        return jsonify({'error': 'No answer found'}), 404

    data = request.get_json()
    answer.question_id = data['questionId']
    answer.answer_text = data['answerText']
    answer.correct = data['correct']

    db.session.commit()
    return jsonify({'message': 'Answer updated'}), 200

@answers.route('/api/answers/<id>', methods=['DELETE'])
@auth_required(minimum_role=UserRole.editor)
def delete_answer(current_user, id):
    answer = Answer.query.filter_by(id=id).first()

    if not answer:
        return jsonify({'error': 'No answer found'}), 404

    db.session.delete(answer)
    db.session.commit()
    return jsonify({'message': 'The answer has been deleted'}), 204
