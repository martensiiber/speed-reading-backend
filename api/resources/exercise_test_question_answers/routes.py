from flask import Blueprint, jsonify, request
from api import db
from api.models import TestQuestionAnswer, Question

test_question_answers = Blueprint('test_question_answers', __name__)

@test_question_answers.route('/api/testQuestionAnswers', methods=['POST'])
def add_test_question_answer():
    data = request.get_json()
    if isinstance(data, list):
        # Many
        for question_answer in data:
            new_question_answer = TestQuestionAnswer(
                test_attempt_id=question_answer['testAttemptId'],
                question_id=question_answer['questionId'],
                answer_id=question_answer['answerId']
            )
            db.session.add(new_question_answer)
        db.session.commit()
        result = jsonify({'message': 'Test question answers added'}), 201
    else:
        # Single
        question_answer = data
        new_question_answer = TestQuestionAnswer(
            test_attempt_id=question_answer['testAttemptId'],
            question_id=question_answer['questionId'],
            answer_id=question_answer['answerId']
        )
        db.session.add(new_question_answer)
        db.session.commit()
        result = jsonify({'message': 'Test question answer added'}), 201

    return result

@test_question_answers.route('/api/testQuestionAnswers', methods=['GET'])
def get_test_question_answers():
    test_attempt_id = request.args.get('testAttemptId')

    test_question_answers = TestQuestionAnswer.query.filter_by(test_attempt_id=test_attempt_id).all()
    output = []
    for test_question_answer in test_question_answers:
        question_answer_data = {}
        question_answer_data['id'] = test_question_answer.id
        question_answer_data['answerId'] = test_question_answer.answer_id
        question = Question.query.filter_by(id=test_question_answer.question_id).first()
        question_answer_data['questionId'] = question.id
        question_answer_data['questionText'] = question.question_text
        question_answer_data['questionCategory'] = question.category
        answers = []
        for answer in question.answers:
            answer_data = {}
            answer_data['id'] = answer.id
            answer_data['answerText'] = answer.answer_text
            answer_data['correct'] = answer.correct
            answers.append(answer_data)
        question_answer_data['answers'] = answers
        output.append(question_answer_data)

    return jsonify(output), 200
