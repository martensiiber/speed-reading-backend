# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, Question, QuestionCategory, Answer, User, ExerciseAttempt, TestAttempt, TestQuestionAnswer
from tests.utils import auth
from tests.data.resources import reading_text_data_est
import credentials

class TestExerciseTestQuestionAnswers(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.commit()
        db.session.add(Question(
            reading_text_id=1,
            category=QuestionCategory.question,
            question_text='Test question'
        ))
        db.session.commit()
        db.session.add(Answer(
            question_id=1,
            answer_text='Test answer',
            correct=True
        ))
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=1,
            reading_text_id=1,
            modification='default',
            start_time=datetime.now(),
            result={
                'wordsPerMinute': 200
            }
        ))
        db.session.commit()
        db.session.add(TestAttempt(
            exercise_attempt_id=1,
            start_time=datetime.now()
        ))
        db.session.add(TestAttempt(
            exercise_attempt_id=1,
            start_time=datetime.now()
        ))
        db.session.commit()
        db.session.add(TestQuestionAnswer(
            test_attempt_id=1,
            question_id=1,
            answer_id=1
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_one_answer_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_question_answer_data = {
            'testAttemptId': 2,
            'questionId': 1,
            'answerId': 1
        }
        response = self.client.post('/api/testQuestionAnswers', json=test_question_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Test question answer added'
        })

    def test_add_multiple_answers_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        test_question_answer_data = [{
            'testAttemptId': 2,
            'questionId': 1,
            'answerId': 1
        }]
        response = self.client.post('/api/testQuestionAnswers', json=test_question_answer_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Test question answers added'
        })

    def test_get_test_question_answers_returns_response(self):
        response = self.client.get('/api/testQuestionAnswers?testAttemptId=1')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'answerId': 1,
            'questionId': 1,
            'questionText': 'Test question',
            'questionCategory': 'question',
            'answers': [{
                'id': 1,
                'answerText': 'Test answer',
                'correct': True
            }]
        }])

if __name__ == '__main__':
    unittest.main()
