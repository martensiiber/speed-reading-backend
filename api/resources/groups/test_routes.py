# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import Group, User
from tests.utils import auth
import credentials

class TestGroups(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(Group(
            name='First group'
        ))
        db.session.add(Group(
            name='Second group'
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_group_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'Third group',
        }
        response = self.client.post('/api/groups', json=group_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 3,
            'message': 'New group created'
        })

    def test_add_group_adds_teacher_id(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'Third group',
        }
        self.client.post('/api/groups', json=group_data, headers={'x-access-token': token})
        admin_user = User.query.filter_by(email=credentials.admin['username']).first()
        third_group = Group.query.filter_by(id=3).first()
        self.assertEqual(third_group.teacher_id, admin_user.public_id)

    def test_add_existing_group_returns_error(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'First group',
        }
        response = self.client.post('/api/groups', json=group_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Group already exists'
        })

    def test_get_groups_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/groups', headers={'x-access-token': token})
        first_group = Group.query.filter_by(id=1).first()
        second_group = Group.query.filter_by(id=2).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 2,
            'name': 'Second group',
            'creationDate': second_group.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'userCount': 0,
            'teacherId': None
        }, {
            'id': 1,
            'name': 'First group',
            'creationDate': first_group.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'userCount': 0,
            'teacherId': None
        }])

    def test_get_one_group_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/groups/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'id': 1,
            'name': 'First group'
        })

    def test_get_unknown_group_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/groups/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No group found'
        })

    def test_update_group_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'Changed group name',
        }
        response = self.client.put('/api/groups/1', json=group_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Group updated'
        })

    def test_update_group_returns_error(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'Second group',
        }
        response = self.client.put('/api/groups/1', json=group_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Group already exists'
        })

    def test_update_unknown_group_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        group_data = {
            'name': 'Changed group name',
        }
        response = self.client.put('/api/groups/0', json=group_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No group found'
        })

    def test_delete_group_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/groups/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 204)

    def test_delete_unknown_group_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.delete('/api/groups/0', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No group found'
        })

if __name__ == '__main__':
    unittest.main()
