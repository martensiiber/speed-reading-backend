from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, Group
from api.decorators import auth_required

groups = Blueprint('groups', __name__)

@groups.route('/api/groups', methods=['GET'])
def get_all_groups():
    groups = Group.query.order_by(Group.created_at.desc()).all()

    output = []
    for group in groups:
        group_data = {}
        group_data['id'] = group.id
        group_data['name'] = group.name
        group_data['creationDate'] = group.created_at
        group_data['userCount'] = len(group.users)
        group_data['teacherId'] = group.teacher_id
        output.append(group_data)
    return jsonify(output), 200

@groups.route('/api/groups/<id>', methods=['GET'])
@auth_required()
def get_one_group(current_user, id):
    group = Group.query.filter_by(id=id).first()

    if not group:
        return jsonify({'error': 'No group found'}), 404

    group_data = {}
    group_data['id'] = group.id
    group_data['name'] = group.name

    return jsonify(group_data), 200

@groups.route('/api/groups', methods=['POST'])
@auth_required(minimum_role=UserRole.teacher)
def create_group(current_user):
    data = request.get_json()
    if Group.query.filter_by(name=data['name']).first():
        return jsonify({'error': 'Group already exists'}), 400
    new_group = Group(
        name=data['name'],
        teacher_id=current_user.public_id
    )
    db.session.add(new_group)
    db.session.commit()
    return jsonify({'message': 'New group created', 'id': new_group.id}), 201, {'Location': '/groups/{}'.format(new_group.id)}

@groups.route('/api/groups/<id>', methods=['PUT'])
@auth_required(minimum_role=UserRole.teacher)
def update_group(current_user, id):
    group = Group.query.filter_by(id=id).first()

    if not group:
        return jsonify({'error': 'No group found'}), 404

    data = request.get_json()
    if Group.query.filter_by(name=data['name']).first():
        return jsonify({'error': 'Group already exists'}), 400
    group.name = data['name']

    db.session.commit()
    return jsonify({'message': 'Group updated'}), 200

@groups.route('/api/groups/<id>', methods=['DELETE'])
@auth_required(minimum_role=UserRole.admin)
def delete_group(current_user, id):
    group = Group.query.filter_by(id=id).first()

    if not group:
        return jsonify({'error': 'No group found'}), 404

    db.session.delete(group)
    db.session.commit()
    return jsonify({'message': 'The group has been deleted'}), 204
