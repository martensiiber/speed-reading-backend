# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, ExerciseAttempt, User, ApplicationStatistics
from tests.utils import auth
from tests.data.resources import reading_text_data_est
import credentials

class TestExerciseAttempts(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=1,
            reading_text_id=1,
            modification='default',
            start_time=datetime.now(),
            statistics={
                'textReadingAttemptCount': 1
            }
        ))
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=6,
            modification='default',
            start_time=datetime.now()
        ))
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.exercise_attempt_count = 2
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_exercise_attempt_returns_response(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {'wordsPerMinute': 200}
        }
        response = self.client.post('/api/exerciseAttempts', json=exercise_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 3,
            'message': 'Exercise attempt added'
        })

    def test_add_exercise_attempt_increases_reading_attempt_count(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {'wordsPerMinute': 200}
        }
        self.client.post('/api/exerciseAttempts', json=exercise_data)
        reading_text = ReadingText.query.filter_by(id=1).first()
        self.assertEqual(reading_text.statistics['totalReadingAttemptCount'], 2)

    def test_add_exercise_attempt_saves_settings(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {'wordsPerMinute': 200}
        }
        self.client.post('/api/exerciseAttempts', json=exercise_data)
        user = User.query.filter_by(email=credentials.admin['username']).first()
        self.assertEqual(user.settings, {'wordsPerMinute': 200})

    def test_add_exercise_attempt_updates_settings(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        first_exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {
                'textOptions': {
                    'font': 'Arial',
                    'fontSize': 14
                },
                'wordsPerMinute': 200
            }
        }
        self.client.post('/api/exerciseAttempts', json=first_exercise_data)
        second_exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {
                'textOptions': {
                    'fontSize': 15
                },
                'wordsPerMinute': 250,
            }
        }
        self.client.post('/api/exerciseAttempts', json=second_exercise_data)
        user = User.query.filter_by(email=credentials.admin['username']).first()
        self.assertEqual(user.settings, {
            'textOptions': {
                'font': 'Arial',
                'fontSize': 15
            },
            'wordsPerMinute': 250
        })

    def test_add_exercise_attempt_updates_application_statistics(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'modification': 'default',
            'startTime': datetime.now().isoformat() + 'Z',
            'settings': {'wordsPerMinute': 200}
        }
        self.client.post('/api/exerciseAttempts', json=exercise_data)
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.exercise_attempt_count, 3)

    def test_get_all_exercise_attempts_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/exerciseAttempts', headers={'x-access-token': token})
        user = User.query.filter_by(email=credentials.admin['username']).first()
        first_exercise_attempt = ExerciseAttempt.query.filter_by(id=1).first()
        second_exercise_attempt = ExerciseAttempt.query.filter_by(id=2).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'userId': user.public_id,
            'exerciseId': 1,
            'readingTextId': 1,
            'textReadingAttemptCount': 1,
            'readingTextTitle': 'Test pealkiri',
            'modification': 'default',
            'startTime': first_exercise_attempt.start_time.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'settings': None,
            'result': None
        }, {
            'id': 2,
            'userId': user.public_id,
            'exerciseId': 6,
            'readingTextId': None,
            'textReadingAttemptCount': None,
            'readingTextTitle': None,
            'modification': 'default',
            'startTime': second_exercise_attempt.start_time.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'settings': None,
            'result': None
        }])

    def test_get_user_exercise_attempts_returns_response(self):
        demo_user = User.query.filter_by(email=credentials.demo['username']).first()
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/exerciseAttempts?userId=' + demo_user.public_id, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [])

    def test_get_group_exercise_attempts_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/exerciseAttempts?groupId=1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [])

    def test_get_exercise_attempts_grouped_by_user_id_returns_response(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/exerciseAttempts?groupBy=userId', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json[user.public_id]), 2)

    def test_patch_exercise_attempt_returns_response(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'result': {
                'elapsedTime': 100000
            }
        }
        response = self.client.patch('/api/exerciseAttempts/1', json=exercise_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Exercise attempt patched'
        })

    def test_unknown_exercise_attempt_returns_not_found(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'result': {
                'elapsedTime': 100000
            }
        }
        response = self.client.patch('/api/exerciseAttempts/0', json=exercise_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No exercise attempt found'
        })

    def test_patch_reading_exercise_attempt_updates_application_statistics(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        exercise_data = {
            'result': {
                'elapsedTime': 100000
            }
        }
        self.client.patch('/api/exerciseAttempts/1', json=exercise_data)
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.reading_exercise_time, 100000)

    def test_patch_reading_attempt_updates_application_statistics(self):
        exercise_data = {
            'result': {
                'elapsedTime': 50000
            }
        }
        self.client.patch('/api/exerciseAttempts/2', json=exercise_data)
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.help_exercise_time, 50000)

    def test_patch_nothing_on_reading_attempt_returns_error(self):
        exercise_data = {}
        response = self.client.patch('/api/exerciseAttempts/1', json=exercise_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Nothing to be patched'
        })

if __name__ == '__main__':
    unittest.main()
