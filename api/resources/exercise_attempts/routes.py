from flask import Blueprint, jsonify, request
from datetime import datetime
from sqlalchemy import and_, true
from sqlalchemy.orm.query import Bundle
from api import db
from api.models import UserRole, ExerciseAttempt, ReadingText, User, ApplicationStatistics
from api.decorators import auth_required
from api.resources.utility.utils import role_permissions, merge_dict

exercise_attempts = Blueprint('exercise_attempts', __name__)
get_exercise_attempts_bundle = Bundle(
    'exercise_attempt',
    ExerciseAttempt.id, ExerciseAttempt.user_id, ExerciseAttempt.exercise_id,
    ExerciseAttempt.reading_text_id, ExerciseAttempt.modification,
    ExerciseAttempt.start_time, ExerciseAttempt.settings, ExerciseAttempt.result
)

@exercise_attempts.route('/api/exerciseAttempts', methods=['GET'])
@auth_required(minimum_role=UserRole.student)
def get_all_exercise_attempts(current_user):
    user_id = request.args.get('userId')
    group_id = request.args.get('groupId')
    group_by = request.args.get('groupBy')
    embed = request.args.get('embed')

    filters = [ExerciseAttempt.save == true()]
    if user_id is not None:
        filters.append(ExerciseAttempt.user_id == user_id)
    if group_id is not None:
        filters.append(User.group_id == group_id)

    if filters:
        attempts = db.session.query(ExerciseAttempt).join(User).filter(*filters).order_by(ExerciseAttempt.created_at.asc()).all()
    else:
        if role_permissions[current_user.role] < role_permissions['teacher']:
            return jsonify({'error': 'You do not have required permission'}), 403
        attempts = ExerciseAttempt.query.order_by(ExerciseAttempt.created_at.asc()).all()

    output = [] if group_by is None else {}
    for attempt in attempts:
        attempt_data = {}
        attempt_data['id'] = attempt.id
        attempt_data['userId'] = attempt.user_id
        attempt_data['exerciseId'] = attempt.exercise_id
        reading_text_id = attempt.reading_text_id
        if reading_text_id:
            attempt_data['readingTextId'] = reading_text_id
            attempt_data['textReadingAttemptCount'] = attempt.statistics['textReadingAttemptCount'] if attempt.statistics != None else 0
            attempt_data['readingTextTitle'] = attempt.reading_text.title
        else:
            attempt_data['readingTextId'] = None
            attempt_data['textReadingAttemptCount'] = None
            attempt_data['readingTextTitle'] = None
        attempt_data['modification'] = attempt.modification
        attempt_data['startTime'] = attempt.start_time
        attempt_data['settings'] = attempt.settings
        attempt_data['result'] = attempt.result
        if embed == 'test':
            test_attempt = attempt.test
            if test_attempt:
                test_attempt_data = {}
                test_attempt_data['startTime'] = test_attempt.start_time
                test_attempt_data['result'] = test_attempt.result
                attempt_data['test'] = test_attempt_data
            else:
                attempt_data['test'] = None
        if group_by == 'userId':
            if attempt.user_id in output:
                output[attempt.user_id].append(attempt_data)
            else:
                output[attempt.user_id] = [attempt_data]
        else:
            output.append(attempt_data)

    if group_by == 'userId' and group_id is not None:
        group_users = db.session.query(User.public_id).filter_by(group_id=group_id).all()
        for group_user_id, in group_users:
            if group_user_id not in output:
                output[group_user_id] = []

    return jsonify(output), 200

@exercise_attempts.route('/api/exerciseAttempts', methods=['POST'])
def add_exercise_attempt():
    data = request.get_json()
    new_exercise_attempt = ExerciseAttempt(
        user_id=data['userId'],
        exercise_id=data['exerciseId'],
        modification=data['modification'],
        start_time=datetime.strptime(data['startTime'], '%Y-%m-%dT%H:%M:%S.%fZ')
    )
    if 'readingTextId' in data:
        new_exercise_attempt.reading_text_id = data['readingTextId']
        text_reading_attempt_count = ExerciseAttempt.query.filter(and_(
            ExerciseAttempt.reading_text_id == new_exercise_attempt.reading_text_id,
            ExerciseAttempt.start_time <= new_exercise_attempt.start_time,
            ExerciseAttempt.user_id == new_exercise_attempt.user_id
        )).count()
        new_exercise_attempt.statistics = {}
        new_exercise_attempt.statistics['textReadingAttemptCount'] = text_reading_attempt_count + 1
        reading_text = ReadingText.query.filter_by(id=new_exercise_attempt.reading_text_id).first()
        reading_text.statistics.update({
            'totalReadingAttemptCount': reading_text.statistics['totalReadingAttemptCount'] + 1
        })
    if 'save' in data:
        new_exercise_attempt.save = data['save']
    if 'settings' in data:
        settings = data['settings']
        user = User.query.filter_by(public_id=new_exercise_attempt.user_id).first()
        if user.settings is None:
            user.settings = settings
        else:
            user.settings = merge_dict(user.settings, settings)
        new_exercise_attempt.settings = settings
    db.session.add(new_exercise_attempt)
    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.exercise_attempt_count += 1
    db.session.commit()
    return jsonify({'message': 'Exercise attempt added', 'id': new_exercise_attempt.id}), 201, {'Location': '/exerciseAttempts/{}'.format(new_exercise_attempt.id)}

@exercise_attempts.route('/api/exerciseAttempts/<id>', methods=['PATCH'])
def patch_exercise_attempt(id):
    exercise_attempt = ExerciseAttempt.query.filter_by(id=id).first()

    if not exercise_attempt:
        return jsonify({'error': 'No exercise attempt found'}), 404

    data = request.get_json()
    if 'settings' not in data and 'result' not in data:
        return jsonify({'error': 'Nothing to be patched'}), 400
    if 'settings' in data:
        exercise_attempt.settings = data['settings']
    if 'result' in data:
        exercise_attempt.result = data['result']

    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()

    elapsed_time = exercise_attempt.result['elapsedTime']
    if exercise_attempt.exercise.type == 'reading':
        application_statistics.reading_exercise_time += elapsed_time
    elif  exercise_attempt.exercise.type == 'help':
        application_statistics.help_exercise_time += elapsed_time

    db.session.commit()
    return jsonify({'message': 'Exercise attempt patched'}), 200
