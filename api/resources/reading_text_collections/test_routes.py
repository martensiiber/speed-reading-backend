# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from tests.utils import auth
import credentials

class TestReadingTextCollections(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_reading_text_collection_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        reading_collection_data = {
            'title': 'New collection'
        }
        response = self.client.post('/api/collections', json=reading_collection_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Reading text collection added'
        })

    def test_get_reading_text_collections_returns_response(self):
        response = self.client.get('/api/collections')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'title': 'Kirjandus'
        }, {
            'id': 2,
            'title': 'Kultuur'
        }, {
            'id': 3,
            'title': 'Loodus'
        }, {
            'id': 4,
            'title': 'Tervis'
        }, {
            'id': 5,
            'title': 'Ühiskond'
        }])

if __name__ == '__main__':
    unittest.main()
