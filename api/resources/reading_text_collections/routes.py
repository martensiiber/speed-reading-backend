from flask import Blueprint, jsonify, request
from api import db
from api.models import UserRole, ReadingTextCollection
from api.decorators import auth_required

reading_text_collections = Blueprint('reading_text_collections', __name__)

# Reading text collection resource
@reading_text_collections.route('/api/collections', methods=['GET'])
def get_all_reading_text_collections():
    reading_text_collections = ReadingTextCollection.query.all()

    output = []
    for reading_text_collection in reading_text_collections:
        reading_text_collection_data = {}
        reading_text_collection_data['id'] = reading_text_collection.id
        reading_text_collection_data['title'] = reading_text_collection.title
        output.append(reading_text_collection_data)
    return jsonify(output), 200

@reading_text_collections.route('/api/collections', methods=['POST'])
@auth_required(minimum_role=UserRole.editor)
def add_reading_text_collection(current_user):
    data = request.get_json()
    new_reading_text_collection = ReadingTextCollection(
        title=data['title']
    )
    db.session.add(new_reading_text_collection)
    db.session.commit()
    return jsonify({'message': 'Reading text collection added'}), 201, {'Location': '/collections/{}'.format(new_reading_text_collection.id)}
