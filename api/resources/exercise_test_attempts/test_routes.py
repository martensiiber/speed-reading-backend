# -*- coding: utf-8 -*-
import unittest
from datetime import datetime
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import ReadingText, Question, QuestionCategory, Answer, TestQuestionAnswer, ExerciseAttempt, TestAttempt, User, ApplicationStatistics
from tests.utils import auth
from tests.data.resources import reading_text_data_est
import credentials

class TestTestAttempts(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        user = User.query.filter_by(email=credentials.admin['username']).first()
        db.session.add(ReadingText(**reading_text_data_est))
        db.session.commit()
        db.session.add(Question(
            reading_text_id=1,
            category=QuestionCategory.question,
            question_text='Test question'
        ))
        db.session.commit()
        db.session.add(Answer(
            question_id=1,
            answer_text='Test answer',
            correct=True
        ))
        db.session.add(ExerciseAttempt(
            user_id=user.public_id,
            exercise_id=1,
            reading_text_id=1,
            modification='default',
            start_time=datetime.now(),
            result={
                'wordsPerMinute': 200
            }
        ))
        db.session.commit()
        db.session.add(TestAttempt(
            exercise_attempt_id=1,
            start_time=datetime.now(),
            result={
                'elapsedTime': 40000
            }
        ))
        db.session.commit()
        db.session.add(TestQuestionAnswer(
            test_attempt_id=1,
            question_id=1,
            answer_id=1
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_exercise_test_attempt_returns_response(self):
        test_data = {
            'exerciseAttemptId': 1,
            'startTime': datetime.now().isoformat() + 'Z',
        }
        response = self.client.post('/api/testAttempts', json=test_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'id': 2,
            'message': 'Test attempt added'
        })

    def test_get_exercise_test_attempts_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/testAttempts', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        exercise_test_attempt = TestAttempt.query.filter_by(id=1).first()
        self.assertEqual(response.json, [{
            'id': 1,
            'exerciseAttemptId': 1,
            'startTime': exercise_test_attempt.start_time.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'result': {
                'elapsedTime': 40000
            }
        }])

    def test_one_exercise_test_attempt_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/testAttempts/1', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        exercise_test_attempt = TestAttempt.query.filter_by(id=1).first()
        self.assertEqual(response.json, {
            'id': 1,
            'exerciseAttemptId': 1,
            'startTime': exercise_test_attempt.start_time.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'result': {
                'elapsedTime': 40000
            }
        })

    def test_one_reevaluated_exercise_test_attempt_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/testAttempts/1/re-evaluate', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        exercise_test_attempt = TestAttempt.query.filter_by(id=1).first()
        self.assertEqual(response.json, {
            'id': 1,
            'exerciseAttemptId': 1,
            'startTime': exercise_test_attempt.start_time.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'result': {
                'elapsedTime': 40000,
                'total': 1,
                'correct': 1,
                'incorrect': 0,
                'unanswered': 0,
                'testResult': 1.0,
                'comprehensionResult': 1.0,
                'comprehensionPerMinute': 200.0
            }
        })

    def test_patch_reevaluated_exercise_test_attempt_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.patch('/api/testAttempts/1/re-evaluate', headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Test re-evaluated attempt patched',
            'result': {
                'elapsedTime': 40000,
                'total': 1,
                'correct': 1,
                'incorrect': 0,
                'unanswered': 0,
                'testResult': 1.0,
                'comprehensionResult': 1.0,
                'comprehensionPerMinute': 200.0
            }
        })

    def test_patch_exercise_test_attempt_returns_response(self):
        exercise_test_data = {
            'result': {
                'elapsedTime': 50000,
            }
        }
        response = self.client.patch('/api/testAttempts/1', json=exercise_test_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Test attempt patched',
            'result': {
                'elapsedTime': 50000,
                'total': 1,
                'correct': 1,
                'incorrect': 0,
                'unanswered': 0,
                'testResult': 1.0,
                'comprehensionResult': 1.0,
                'comprehensionPerMinute': 200.0
            }
        })

    def test_patch_unknown_exercise_test_attempt_returns_not_found(self):
        exercise_test_data = {
            'result': {
                'elapsedTime': 50000,
            }
        }
        response = self.client.patch('/api/testAttempts/0', json=exercise_test_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No test attempt found'
        })

    def test_patch_exercise_test_attempt_updates_application_statistics(self):
        exercise_test_data = {
            'result': {
                'elapsedTime': 50000,
            }
        }
        self.client.patch('/api/testAttempts/1', json=exercise_test_data)
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.test_time, 50000)

if __name__ == '__main__':
    unittest.main()
