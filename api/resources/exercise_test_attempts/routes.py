from flask import Blueprint, jsonify, request
from datetime import datetime
from api import db
from api.decorators import auth_required
from api.models import UserRole, TestAttempt, ExerciseAttempt, TestQuestionAnswer, BlankAnswerCategory, TestBlankAnswer, Answer, ApplicationStatistics
from api.resources.utility.utils import evaluate_test_attempt

test_attempts = Blueprint('test_attempts', __name__)

@test_attempts.route('/api/testAttempts', methods=['GET'])
@auth_required(minimum_role=UserRole.teacher)
def get_all_test_attempts(current_user):
    test_attempts = TestAttempt.query.all()

    output = []
    for test_attempt in test_attempts:
        test_attempt_data = {}
        test_attempt_data['id'] = test_attempt.id
        test_attempt_data['exerciseAttemptId'] = test_attempt.exercise_attempt_id
        test_attempt_data['startTime'] = test_attempt.start_time
        test_attempt_data['result'] = test_attempt.result
        output.append(test_attempt_data)

    return jsonify(output), 200

@test_attempts.route('/api/testAttempts/<id>', methods=['GET'])
@auth_required()
def get_one_test_attempt(current_user, id):
    test_attempt = TestAttempt.query.filter_by(id=id).first()

    test_attempt_data = {}
    test_attempt_data['id'] = test_attempt.id
    test_attempt_data['exerciseAttemptId'] = test_attempt.exercise_attempt_id
    test_attempt_data['startTime'] = test_attempt.start_time
    test_attempt_data['result'] = test_attempt.result

    return jsonify(test_attempt_data), 200

@test_attempts.route('/api/testAttempts/<id>/re-evaluate', methods=['GET'])
@auth_required()
def get_one_reevaluated_test_attempt(current_user, id):
    test_attempt = TestAttempt.query.filter_by(id=id).first()

    if test_attempt.result is None:
        return jsonify({'error': 'Nothing to re-evaluate'}), 400

    test_attempt_data = {}
    test_attempt_data['id'] = test_attempt.id
    test_attempt_data['exerciseAttemptId'] = test_attempt.exercise_attempt_id
    test_attempt_data['startTime'] = test_attempt.start_time
    result_data = test_attempt.result
    result_data.update(evaluate_test_attempt(test_attempt))
    test_attempt_data['result'] = result_data

    return jsonify(test_attempt_data), 200

@test_attempts.route('/api/testAttempts/<id>/re-evaluate', methods=['PATCH'])
@auth_required()
def patch_reevaluate_test_attempt(current_user, id):
    test_attempt = TestAttempt.query.filter_by(id=id).first()

    if test_attempt.result is None:
        return jsonify({'error': 'Nothing to re-evaluate'}), 400

    result_data = test_attempt.result
    result_data.update(evaluate_test_attempt(test_attempt))
    test_attempt.result = result_data

    db.session.commit()
    return jsonify({'message': 'Test re-evaluated attempt patched', 'result': test_attempt.result}), 200

@test_attempts.route('/api/testAttempts', methods=['POST'])
def add_test_attempt():
    data = request.get_json()
    new_test_attempt = TestAttempt(
        exercise_attempt_id=data['exerciseAttemptId'],
        start_time=datetime.strptime(data['startTime'], '%Y-%m-%dT%H:%M:%S.%fZ')
    )
    db.session.add(new_test_attempt)
    db.session.commit()

    return jsonify({'message': 'Test attempt added', 'id': new_test_attempt.id}), 201, {'Location': '/testAttempts/{}'.format(new_test_attempt.id)}

@test_attempts.route('/api/testAttempts/<id>', methods=['PATCH'])
def patch_test_attempt(id):
    test_attempt = TestAttempt.query.filter_by(id=id).first()

    if not test_attempt:
        return jsonify({'error': 'No test attempt found'}), 404

    data = request.get_json()
    if 'result' not in data:
        return jsonify({'error': 'Nothing to be patched'}), 400

    result_data = data['result']
    result_data.update(evaluate_test_attempt(test_attempt))
    test_attempt.result = result_data

    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.test_time += test_attempt.result['elapsedTime']

    db.session.commit()
    return jsonify({'message': 'Test attempt patched', 'result': test_attempt.result}), 200
