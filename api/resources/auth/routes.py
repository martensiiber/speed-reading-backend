from flask import Blueprint, jsonify, request, current_app
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
import jwt
import uuid
import credentials
from api import db
from api.models import User, UserRole, ApplicationStatistics
from api.decorators import auth_required
from api.resources.utility.utils import generate_password, send_password_email

auth = Blueprint('auth', __name__)

@auth.route('/api/register', methods=['POST'])
def register():
    data = request.get_json()
    email = data['email'].lower()

    if User.query.filter_by(email=email).first():
        return jsonify({'error': 'User already exists'}), 400

    generated_password = generate_password()
    hashed_password = generate_password_hash(generated_password, method='sha256')

    new_user = User(
        public_id=str(uuid.uuid4()),
        email=email,
        password=hashed_password,
        role=UserRole.student
    )
    if 'groupId' in data:
        new_user.group_id = data['groupId']

    send_password_email(email, email, generated_password)


    db.session.add(new_user)

    application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
    application_statistics.user_count += 1

    db.session.commit()
    return jsonify({'message': 'Account registered, password sent with email'}), 201, {'Location': '/users/{}'.format(new_user.public_id)}

@auth.route('/api/login', methods=['GET'])
def login():
    auth = request.authorization
    email = auth.username.lower()
    if not auth or not email or (not email == credentials.demo['username'] and not auth.password):
        return jsonify({'error': 'Authentication missing'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'}

    user = User.query.filter_by(email=email).first()

    expiresIn = 360  # minutes

    if not user:
        return jsonify({'error': 'User cannot be found'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'}

    if user.email == credentials.demo['username'] or check_password_hash(user.password, auth.password):
        token = jwt.encode({'publicId': user.public_id, 'exp': datetime.utcnow() + timedelta(minutes=expiresIn)}, current_app.config['SECRET_KEY'], algorithm='HS256')
        user.last_login = datetime.utcnow()
        db.session.commit()
        return jsonify({'token': token.decode('UTF-8'), 'userId': user.public_id, 'expiresIn': expiresIn * 60})

    return jsonify({'error': 'Incorrect password'}), 401, {'WWW-Authenticate': 'Basic realm="Login required!"'}

@auth.route('/api/changePassword', methods=['GET'])
@auth_required()
def change_password(current_user):
    if current_user.email == credentials.demo['username']:
        return jsonify({'error': 'Password can\'t be changed'}), 400
    auth = request.authorization
    if not auth or not auth.password:
        return jsonify({'error': 'Password missing'}), 400
    [old_password, new_password] = auth.password.split('_')
    if not old_password or not new_password:
        return jsonify({'error': 'Password missing'}), 400

    if check_password_hash(current_user.password, old_password):
        current_user.password = generate_password_hash(new_password, method='sha256')
        db.session.commit()
        return jsonify({'message': 'Password changed'}), 200

    return jsonify({'error': 'Incorrect old password'}), 400
