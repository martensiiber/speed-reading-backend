# -*- coding: utf-8 -*-
import unittest
from base64 import b64encode
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import User, ApplicationStatistics
from tests.utils import auth
import credentials

class TestAuth(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        application_statistics.user_count = 2
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_register_returns_response(self):
        register_data = {
            'email': 'test@test.com'
        }
        response = self.client.post('/api/register', json=register_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Account registered, password sent with email'
        })

    def test_register_existing_returns_error(self):
        register_data = {
            'email': credentials.admin['username']
        }
        response = self.client.post('/api/register', json=register_data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'User already exists'
        })

    def test_register_adds_user(self):
        register_data = {
            'email': 'test@test.com'
        }
        self.client.post('/api/register', json=register_data)
        user = User.query.filter_by(email='test@test.com').first()
        self.assertIsNotNone(user)

    def test_register_updates_application_statistics(self):
        register_data = {
            'email': 'test@test.com'
        }
        self.client.post('/api/register', json=register_data)
        application_statistics = ApplicationStatistics.query.filter_by(id=1).first()
        self.assertEqual(application_statistics.user_count, 3)

    def test_login_returns_response(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            credentials.admin['password']
        ).encode('UTF-8')).decode('UTF-8')
        response = self.client.get('/api/login', headers={'Authorization': auth_header})
        user = User.query.filter_by(email=credentials.admin['username']).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json['expiresIn'], 21600)
        self.assertEqual(len(response.json['token']), 172)
        self.assertEqual(response.json['userId'], user.public_id)

    def test_login_no_credentials_returns_error(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format('', '').encode('UTF-8')).decode('UTF-8')
        response = self.client.get('/api/login', headers={'Authorization': auth_header})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json, {
            'error': 'Authentication missing'
        })

    def test_login_unknown_user_returns_error(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            'test@test.com',
            credentials.admin['password']
        ).encode('UTF-8')).decode('UTF-8')
        response = self.client.get('/api/login', headers={'Authorization': auth_header})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json, {
            'error': 'User cannot be found'
        })

    def test_login_wrong_password_returns_error(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            'Wrong'
        ).encode('UTF-8')).decode('UTF-8')
        response = self.client.get('/api/login', headers={'Authorization': auth_header})
        self.assertEqual(response.status_code, 401)
        self.assertEqual(response.json, {
            'error': 'Incorrect password'
        })

    def test_change_password_returns_response(self):
        new_password = 'Password'
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            credentials.admin['password'] + '_' + new_password
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Password changed'
        })

    def test_change_demo_user_password_returns_error(self):
        new_password = 'Password'
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.demo['username'],
            credentials.demo['password'] + '_' + new_password
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.demo['username'], credentials.demo['password'])
        response = self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Password can\'t be changed'
        })

    def test_change_password_with_missing_passwords_returns_error(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            ''
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Password missing'
        })

    def test_change_password_with_missing_password_returns_error(self):
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            credentials.admin['password'] + '_'
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Password missing'
        })

    def test_change_password_with_wrong_password_returns_error(self):
        new_password = 'Password'
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            'Wrong' + '_' + new_password
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Incorrect old password'
        })

    def test_change_password_updates_password(self):
        new_password = 'Password'
        auth_header = "Basic " + b64encode("{0}:{1}".format(
            credentials.admin['username'],
            credentials.admin['password'] + '_' + new_password
        ).encode('UTF-8')).decode('UTF-8')
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        self.client.get('/api/changePassword', headers={
            'x-access-token': token,
            'Authorization': auth_header
        })
        token = auth(self.client, credentials.admin['username'], new_password)
        self.assertIsNotNone(token)

if __name__ == '__main__':
    unittest.main()
