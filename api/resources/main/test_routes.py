# -*- coding: utf-8 -*-
import unittest
from pathlib import Path
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.decorators import ignore_warnings

class TestMain(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_api_endpoints_returns_response(self):
        response = self.client.get('/api')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 74)

    def test_heartbeat_returns_response(self):
        response = self.client.get('/api/heartbeat')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'status': 'healthy'
        })

    def test_analyze_estonian_text_returns_response(self):
        text_data = {
            'text': 'Tekst',
            'language': 'estonian'
        }
        response = self.client.post('/api/analyze', json=text_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'characterCount': 5,
            'wordCount': 1,
            'sentenceCount': 1,
            'averageWordLength': 5.0,
            'wordLengthClassRating': 1,
            'averageSentenceLengthInWords': 1.0,
            'averageSentenceLengthInCharacters': 5.0,
            'sentenceLengthClassRating': 1,
            'wordLengths': {'5': 1},
            'sentenceLengths': {'1': 1},
            'wordTypeCounts': {'nimisõna': 1},
            'wordAnalysis': [{
                'word': 'Tekst',
                'posTag': 'S',
                'pos': 'Nimisõna',
                'abstractness': 2,
                'frequency': 971,
                'lemmas': [{
                    'lemma': 'tekst',
                    'abstractness': 2,
                    'frequency': 5351
                }],
                'span': {
                    'start': 0,
                    'end': 5
                }
            }]
        })

    def test_analyze_estonian_word_returns_response(self):
        word_data = {
            'word': 'Tekst',
            'language': 'estonian'
        }
        response = self.client.post('/api/analyzeWord', json=word_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {})

    @ignore_warnings
    def test_analyze_english_word_returns_response(self):
        word_data = {
            'word': 'Text',
            'language': 'english'
        }
        response = self.client.post('/api/analyzeWord', json=word_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'word': 'Text',
            'posTag': 'NN',
            'pos': 'Noun, singular or mass',
            'concreteness': None,
            'frequency': None,
            'frequencyZipf': None,
            'prevalence': None,
            'lemmas': [{
                'lemma': 'text',
                'concreteness': 4.93,
                'frequency': 267,
                'frequencyZipf': 3.72,
                'prevalence': 2.416
            }]
        })

    def test_generate_blank_exercises_returns_response(self):
        text_data = {
            'text': Path('./tests/data/test_input_text_est.txt').read_text(encoding='UTF-8')
        }
        response = self.client.post('/api/generateBlankExercises', json=text_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json), 8)
        for blank_exercise in response.json:
            self.assertEqual(len(blank_exercise['blankExercise']), 3)
            self.assertTrue(isinstance(blank_exercise['correct'], str))

    def test_get_unknown_submitted_screenshot_returns_not_found(self):
        response = self.client.get('/api/submittedScreenshots/not_existing.png')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No screenshot found'
        })

    def test_export_file_returns_response(self):
        export_data = {
            'filename': 'test',
            'filetype': 'xlsx',
            'columns': [],
            'rows': [],
            'headings': []
        }
        response = self.client.post('/api/exportFile', json=export_data)
        self.assertEqual(response.status_code, 200)

if __name__ == '__main__':
    unittest.main()
