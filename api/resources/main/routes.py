import os.path
from time import time
from datetime import datetime
from flask import Blueprint, jsonify, current_app, request, send_file, g
from api import utils, excel, logger
from api.decorators import auth_required
from api.models import UserRole
# from api.resources.utility.data_utils import calculate_exercise_attempt_statistics

main = Blueprint("main", __name__)

KB = 1024
MB = KB ** 2

# Measure response time
@main.before_app_request
def before_request():
    g.start = time()


@main.after_app_request
def after_request(response):
    g.difference = time() - g.start
    timestamp = "[{}]".format(datetime.now().strftime("%d/%b/%Y %H:%M:%S.%f")[:-3])
    response_time = "[{0:.3f}ms]".format(g.difference * 1000)
    if response.content_length is None:
        content_length = "[0kb]"
    elif response.content_length < MB:
        content_length = "[{0:.0f}kb]".format(response.content_length / KB)
    else:
        content_length = "[{0:.2f}mb]".format(response.content_length / MB)
    logger.log(
        "{} {} {} {} {} {} {}".format(
            request.remote_addr,
            timestamp,
            request.method,
            request.full_path,
            response.status_code,
            response_time,
            content_length,
        )
    )
    return response


# API URL
@main.route("/api", methods=["GET"])
def api_endpoints():
    endpoints = []
    for endpoint in current_app.url_map.iter_rules():
        endpoint_data = {}
        endpoint_data["url"] = endpoint.rule
        endpoint_data["methods"] = ", ".join(endpoint.methods)
        endpoints.append(endpoint_data)

    return jsonify(endpoints), 200

# Heartbeat
@main.route("/api/heartbeat", methods=["GET"])
def heartbeat():
    return jsonify({"status": "healthy"}), 200

# User manual
@main.route("/api/kasutusjuhend_est.pdf", methods=["GET"])
def get_manual():
    path = "static/kasutusjuhend_est.pdf"
    if os.path.isfile("api/" + path):
        return send_file(path)
    return jsonify({"error": "No manual found"}), 404


# Text analysis
@main.route("/api/analyze", methods=["POST"])
def analyze_text():
    data = request.get_json()
    text = data["text"]
    language = data["language"]
    result = utils.analyze(text, language)
    return jsonify(result), 200


@main.route("/api/analyzeWord", methods=["POST"])
def analyze_word():
    data = request.get_json()
    word = data["word"]
    if "posTag" in data and data["posTag"] is not None:
        pos_tag = data["posTag"]
    else:
        pos_tag = None
    language = data["language"]
    result = utils.analyze_word(word, pos_tag, language)
    return jsonify(result), 200


# Text blank exercise test
@main.route("/api/generateBlankExercises", methods=["POST"])
def generate_blank_exercises():
    data = request.get_json()
    text = data["text"]
    result = utils.generate_blank_exercises(text)

    return jsonify(result), 200


@main.route("/api/submittedScreenshots/<filename>")
def get_submitted_screenshot(filename):
    path = "static/submitted_screenshots/" + filename
    if os.path.isfile("api/" + path):
        return send_file(path, mimetype="image/png")
    return jsonify({"error": "No screenshot found"}), 404


# Exporting files
@main.route("/api/exportFile", methods=["POST"])
def export_file():
    data = request.get_json()
    file_name = data["filename"]
    file_type = data["filetype"]
    columns = data["columns"]
    rows = data["rows"]
    headings = data["headings"]

    if file_type not in ["csv", "xlsx"]:
        return jsonify({"error": "Unknown file type: " + file_type}), 400

    result = [list(map(lambda column: headings[column], columns))]

    for row in rows:
        row_data = []
        for column in columns:
            row_data.append(row[column])
        result.append(row_data)

    return excel.make_response_from_array(
        array=result, file_type=file_type, file_name=file_name
    )


# Data migrations
@main.route("/api/execute", methods=["GET"])
@auth_required(minimum_role=UserRole.admin)
def execute(current_user):  # pragma: no cover
    # recalculate_application_statistics()
    # analyze_reading_texts()
    # convert_json_attributes()
    # calculate_concentration()
    # calculate_exercise_attempt_statistics()
    return jsonify({"message": "Executed!"}), 200
