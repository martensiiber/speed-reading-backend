from flask import Blueprint, jsonify, request
from base64 import b64decode
import uuid
from api import db
from api.models import UserRole, BugReport
from api.decorators import auth_required
from api.resources.utility.utils import send_bug_report_email

bug_reports = Blueprint('bug_reports', __name__)

@bug_reports.route('/api/bugReports', methods=['GET'])
@auth_required(minimum_role=UserRole.admin)
def get_all_bug_reports(current_user):
    bug_reports = BugReport.query.all()

    output = []
    for bug_report in bug_reports:
        bug_report_data = {}
        bug_report_data['id'] = bug_report.id
        bug_report_data['date'] = bug_report.created_at
        bug_report_data['resolved'] = bug_report.resolved
        bug_report_data['userId'] = bug_report.user_id
        bug_report_data['description'] = bug_report.description
        bug_report_data['version'] = bug_report.version
        bug_report_data['userAgent'] = bug_report.user_agent
        bug_report_data['platform'] = bug_report.platform
        bug_report_data['windowDimensions'] = bug_report.window_dimensions
        bug_report_data['consoleErrors'] = bug_report.console_errors
        bug_report_data['state'] = bug_report.state
        bug_report_data['actions'] = bug_report.actions
        bug_report_data['screenshotFilename'] = bug_report.screenshot_filename
        output.append(bug_report_data)
    return jsonify(output), 200

@bug_reports.route('/api/bugReports', methods=['POST'])
def add_bug_report():
    data = request.get_json()
    new_bug_report = BugReport(
        description=data['description'],
        version=data['version'],
        user_agent=data['userAgent'],
        platform=data['platform'],
        window_dimensions=data['windowDimensions'],
        console_errors=data['consoleErrors'],
        state=data['state'],
        actions=data['actions']
    )
    if 'userId' in data:
        new_bug_report.user_id = data['userId']
    if 'screenshot' in data and data['screenshot'] is not None:
        image_data = b64decode(data['screenshot'])
        # Saving screenshot
        filename = str(uuid.uuid4()) + '.png'
        with open('api/static/submitted_screenshots/' + filename, 'wb') as fh:
            fh.write(image_data)
        new_bug_report.screenshot_filename = filename
    db.session.add(new_bug_report)
    db.session.commit()

    send_bug_report_email(new_bug_report.description)

    return jsonify({'message': 'Bug report added'}), 201, {'Location': '/bugReports/{}'.format(new_bug_report.id)}

@bug_reports.route('/api/bugReports/<id>', methods=['PATCH'])
def update_bug_report(id):

    bug_report = BugReport.query.filter_by(id=id).first()

    if not bug_report:
        return jsonify({'error': 'No bug report found'}), 404

    data = request.get_json()

    if 'resolved' not in data:
        return jsonify({'error': 'Nothing to be patched'}), 400

    bug_report.resolved = data['resolved']

    db.session.commit()
    return jsonify({'message': 'Bug report patched'}), 200
