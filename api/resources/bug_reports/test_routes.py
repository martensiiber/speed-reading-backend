# -*- coding: utf-8 -*-
import unittest
from flask_testing import TestCase
from api import create_app, db
from api.config import get_test_env_config
from api.models import BugReport
from tests.utils import auth
import credentials

class TestBugReports(TestCase):
    def create_app(self):
        return create_app(get_test_env_config())

    def populate_db(self):
        db.session.add(BugReport(
            description='Description',
            version='5.1.0',
            user_agent='Mozilla/5.0',
            platform='Win32',
            window_dimensions=[1366, 768],
            console_errors=[],
            state={},
            actions=[]
        ))
        db.session.commit()

    def setUp(self):
        db.create_all()
        self.populate_db()

    def tearDown(self):
        db.session.close()
        db.drop_all()

    def test_add_bug_report_returns_response(self):
        bug_report_data = {
            'description': 'Description',
            'version': '5.1.0',
            'userAgent': 'Mozilla/5.0',
            'platform': 'Win32',
            'windowDimensions': [1366, 768],
            'consoleErrors': [],
            'state': {},
            'actions': []
        }
        response = self.client.post('/api/bugReports', json=bug_report_data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.json, {
            'message': 'Bug report added'
        })

    def test_get_bug_reports_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        response = self.client.get('/api/bugReports', headers={'x-access-token': token})
        bug_report = BugReport.query.filter_by(id=1).first()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, [{
            'id': 1,
            'date': bug_report.created_at.strftime('%a, %d %b %Y %H:%M:%S GMT'),
            'userId': None,
            'description': 'Description',
            'version': '5.1.0',
            'userAgent': 'Mozilla/5.0',
            'platform': 'Win32',
            'windowDimensions': [1366, 768],
            'consoleErrors': [],
            'state': {},
            'actions': [],
            'screenshotFilename': None,
            'resolved': False
        }])

    def test_patch_bug_report_returns_response(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        bug_report_data = {
            'resolved': True
        }
        response = self.client.patch('/api/bugReports/1', json=bug_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json, {
            'message': 'Bug report patched'
        })

    def test_patch_unknown_bug_report_returns_not_found(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        bug_report_data = {
            'resolved': True
        }
        response = self.client.patch('/api/bugReports/0', json=bug_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json, {
            'error': 'No bug report found'
        })

    def test_patch_nothing_on_bug_report_returns_error(self):
        token = auth(self.client, credentials.admin['username'], credentials.admin['password'])
        bug_report_data = {}
        response = self.client.patch('/api/bugReports/1', json=bug_report_data, headers={'x-access-token': token})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json, {
            'error': 'Nothing to be patched'
        })

if __name__ == '__main__':
    unittest.main()
