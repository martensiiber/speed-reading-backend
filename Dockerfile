FROM continuumio/anaconda3:4.2.0

# Setting environmental variables
ENV LD_LIBRARY_PATH /opt/conda/lib:${LD_LIBRARY_PATH}
ENV PYTHONPATH=/opt/conda/site-packages:${PYTHONPATH}

# Create app directory
RUN mkdir /usr/src/speed-reading-backend
WORKDIR /usr/src/speed-reading-backend

# Update conda
RUN conda update conda

# Create empty conda environment
RUN conda config --add channels conda-forge
RUN conda config --add channels estnltk
RUN conda create --name ci-env python=3.5 estnltk=1.4.1 pipenv --no-default-packages

# Make RUN commands use the environment
RUN echo "source activate ci-env" > ~/.bashrc
ENV PATH /opt/conda/envs/ci-env/bin:$PATH
RUN pipenv --python /opt/conda/envs/ci-env/bin/python --site-packages

# Install app dependencies
COPY Pipfile* ./
RUN pipenv install --dev

# Bundle app source
COPY . .

# Setup nltk
RUN python nltk_setup.py

# Credentials
COPY credentials.py.example ./credentials.py

# Run tests
CMD [ "pipenv", "run", "test" ]
